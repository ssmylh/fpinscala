package datastructures

sealed trait List[+A]
case object Nil extends List[Nothing]
case class Cons[+A](head: A, tail: List[A]) extends List[A]

object List {
  def apply[A](as: A*): List[A] =
    if (as.isEmpty) Nil
    else Cons(as.head, apply(as.tail: _*))
  def sum(ints: List[Int]): Int = ints match {
    case Nil => 0
    case Cons(h, t) => h + sum(t)
  }
  def product(ds: List[Double]): Double = ds match {
    case Nil => 1.0
    case Cons(0.0, _) => 0.0
    case Cons(h, t) => h * product(t)
  }

  // 3.2
  def tail[A](l: List[A]): List[A] = l match {
    case Nil => Nil
    case Cons(_, t) => t
  }

  // 3.3
  def setHead[A](a: A, l: List[A]): List[A] = l match {
    case Nil => Nil
    case Cons(_, t) => Cons(a, t)
  }

  // 3.4
  def drop[A](l: List[A], n: Int): List[A] =
    if (n == 0) l
    else l match {
      case Nil => Nil
      case Cons(_, t) => drop(t, n - 1)
    }

  // 3.5
  def dropWhile[A](l: List[A])(f: A => Boolean): List[A] = l match {
    case Nil => Nil
    case Cons(h, t) =>
      if (f(h)) dropWhile(t)(f)
      else l
  }

  // 3.6 init(List(1,2,3,4)) == List(1,2,3)
  def init[A](l: List[A]): List[A] = l match {
    case Nil => Nil
    case Cons(_, Nil) => Nil
    case Cons(h, t) => Cons(h, init(t))
  }

  def foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B = as match {
    case Nil => z
    case Cons(h, t) => f(h, foldRight(t,z)(f))
  }

  // 3.9
  def length[A](as: List[A]): Int = foldRight(as, 0)((_, acc) => acc + 1)

  // 3.10
  def foldLeft[A, B](as: List[A], z: B) (f: (B, A) => B): B = as match {
    case Nil => z
    case Cons(h, t) => foldLeft(t, f(z, h))(f)
  }

  // 3.11 sum
  def sumViaFoldLeft(ints: List[Int]): Int = foldLeft(ints, 0)(_ + _)
  // 3.11 product
  def productViaFoldLeft(ds: List[Double]): Double = foldLeft(ds, 0.0)(_ * _)
  // 3.11 length
  def lengthViaFoldLeft[A](as: List[A]): Int = foldLeft(as, 0)((acc, _) => acc + 1)

  // 3.12
  def reverse[A](as: List[A]): List[A] = foldLeft(as, Nil: List[A])((acc, a) => Cons(a, acc))

  // 3.13
  // can use reverse, because reverse is implemented by foldLeft
  def foldRightViaFoldLeft[A, B](as: List[A], z: B)(f: (A, B) => B): B = foldLeft(reverse(as), z)((a, b) => f(b, a))

  /*
  f1 = (a,g) => b => g(f(b,a)) として、

  foldLeftViaFoldRight(List(1,2,3) 0)(_ + _) =
  (1, foldRight(List(2,3), (b: B) => b)(f1))
  f1(1, f1(2, foldRight(List(3), (b: B) => b)(f1)))
  f1(1, f1(2, f1(3, foldRight(Nil, (b: B) => b)(f1))))
  f1(1, f1(2, f1(3, (b: B) => b)))

  f1(1, f1(2, b => f(b, 3))) // f1を展開

  g3 = b => f(b, 3) として
  f1(1, f1(2, g3))

  f1(1, b => g3(f(b, 2))) // f1を展開

  g2 = b => g3(f(b, 2)) として
  f1(1, g2) = b => g2(f(b, 1))

  g1 = b => g2(f(b, 1)) として
  g1(0) =
  g2(f(0, 1)) =
  g3(f(f(0,1), 2) =
  f(f(f(0, 1), 2), 3) // 左結合になっている
  */
  def foldLeftViaFoldRight[A,B](l: List[A], z: B)(f: (B,A) => B): B =
    foldRight(l, (b:B) => b)((a,g) => b => g(f(b,a)))(z)

  // 3.14
  def append[A](as1: List[A], as2: List[A]): List[A] = foldRight(as1, as2)((a, acc) => Cons(a, acc))

  // 3.15 flatten
  def concat[A](ass: List[List[A]]): List[A] = foldRight(ass, Nil: List[A])(append)

  // 3.16
  def plusOne(ints: List[Int]): List[Int] = foldRight(ints, Nil: List[Int])((a, acc) => Cons(a + 1, acc))

  // 3.17
  def doubleToString(ds: List[Double]): List[String] = foldRight(ds, Nil: List[String])((d, acc) => Cons(d.toString, acc))

  // 3.18
  def map[A, B](as: List[A])(f: A => B): List[B] = foldRight(as, Nil: List[B])((a, acc) => Cons(f(a), acc))

  // 3.19
  def filter[A](as: List[A])(f: A => Boolean): List[A] = foldRight(as, Nil: List[A]) {
    (a, acc) =>
      if (f(a)) Cons(a, acc)
      else acc
  }

  // 3.20
  def flatMap[A, B](as: List[A])(f: A => List[B]): List[B] = concat(map(as)(f))

  // 3.21
  def filterViaFlatMap[A](as: List[A])(f: A => Boolean): List[A] = flatMap(as)(a => if (f(a)) List(a) else Nil)

  // 3.22
  def addPair(as1: List[Int], as2: List[Int]): List[Int] = (as1, as2) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons(h1, t1), Cons(h2, t2)) => Cons(h1 + h2, addPair(t1, t2))
  }

  // 3.23
  def zipWith[A, B, C](as: List[A], bs: List[B])(f: (A, B) => C): List[C] = (as, bs) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons(h1, t1), Cons(h2, t2)) => Cons(f(h1, h2), zipWith(t1, t2)(f))
  }
}