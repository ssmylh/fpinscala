package datastructures

object Exercise extends App {
  val l = List(1, 2, 3, 4, 5)

  // 3.3
  assert(List.setHead(6, l) == List(6, 2, 3, 4, 5))

  // 3.5
  assert(List.dropWhile(l)(_ < 4) == List(4, 5))

  // 3.6
  assert(List.init(l) == List(1, 2, 3, 4))

  // foldRight
  assert(List.foldRight(l, 0)(_ + _) == 15)

  // 3.8
  // z -> Nil
  // f -> Cons
  assert(List.foldRight(l, Nil: List[Int])(Cons(_, _)) == l)

  // 3.9
  assert(List.length(l) == 5)

  // 3.10
  assert(List.foldLeft(l, 0)(_ + _) == 15)

  // 3.12
  assert(List.reverse(l) == List(5, 4, 3, 2, 1))

  // 3.14
  assert(List.append(l, List(6, 7)) == List(1, 2, 3, 4, 5, 6, 7))

  // 3.15
  assert(List.concat(List(l, List(6, 7))) == List(1, 2, 3, 4, 5, 6, 7))

  // 3.16
  assert(List.plusOne(l) == List(2, 3, 4, 5, 6))

  // 3.17
  assert(List.doubleToString(List(1.0, 2.0)) == List("1.0", "2.0"))

  // 3.18
  assert(List.map(l)(_ + 1) == List.plusOne(l))

  // 3.19
  assert(List.filter(l)(_ % 2 == 0) == List(2, 4))

  // 3.20
  assert(List.flatMap(List(1, 2))(a => List(a, a + 1, a + 2)) == List(1, 2, 3, 2, 3, 4))

  // 3.21
  assert(List.filterViaFlatMap(l)(_ % 2 == 0) == List(2, 4))

  // 3.22
  assert(List.addPair(List(1, 2, 3), List(4, 5, 6)) == List(5, 7, 9))

  // 3.23
  assert(List.zipWith(List(1, 2, 3), List(4, 5, 6))(_ + _) == List(5, 7, 9))

  val t = Branch(Branch(Leaf(1), Leaf(2)), Leaf(3))

  // 3.25
  assert(Tree.size(t) == 5)

  // 3.26
  assert(Tree.maximum(t) == 3)

  // 3.27
  assert(Tree.depth(t) == 2)

  //3.28
  assert(Tree.map(t)(_ + 1) == Branch(Branch(Leaf(2), Leaf(3)), Leaf(4)))

  // 3.29
  assert(Tree.sizeViaFold(t) == 5)
  assert(Tree.maximumViaFold(t) == 3)
  assert(Tree.depthViaFold(t) == 2)
  assert(Tree.mapViaFold(t)(_ + 1) == Branch(Branch(Leaf(2), Leaf(3)), Leaf(4)))

}