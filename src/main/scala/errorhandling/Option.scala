package errorhandling

import scala.{Option => _}

sealed trait Option[+A] {
  // 4.1
  def map[B](f: A => B): Option[B] = this match {
    case None => None
    case Some(a) => Some(f(a))
  }

  def flatMap[B](f: A => Option[B]): Option[B] = map(f) getOrElse(None)

  def getOrElse[B >: A](default: => B): B = this match {
    case None => default
    case Some(a) => a
  }

  def orElse[B >: A](ob: => Option[B]): Option[B] = map(Some(_)).getOrElse(ob)

  def filter(f: A => Boolean): Option[A] = flatMap { a =>
    if (f(a)) this
    else None
  }

  // 4.2
  def mean(xs: Seq[Double]): Option[Double] =
    if (xs.isEmpty) None
    else Some(xs.sum / xs.length)

  def variance(xs: Seq[Double]): Option[Double] = {
    mean(xs) flatMap { m =>
      mean(xs.map(x => math.pow(x - m, 2)))
    }
  }
}

case class Some[+A](get: A) extends Option[A]
case object None extends Option[Nothing]

object Option {
  // 4.3
  def map2[A, B, C](oa: Option[A], ob: Option[B])(f: (A, B) => C): Option[C] = oa.flatMap { a => ob.map(b => f(a, b)) }

  // 4.4
  def sequence[A](oas: List[Option[A]]): Option[List[A]] = oas.foldRight[Option[List[A]]](Some(Nil)){ (oa, acc) =>
    oa.flatMap(a => acc.map(a :: _))
  }

  // 4.5
  // efficiently sequence map result
  def traverse[A, B](as: List[A])(f: A => Option[B]): Option[List[B]] = as.foldRight[Option[List[B]]](Some(Nil)){ (a, acc) =>
    map2(f(a), acc)(_ :: _)
  }
}
