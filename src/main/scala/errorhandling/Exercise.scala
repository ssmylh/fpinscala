package errorhandling

import scala.{ Option => _, Either => _ }

object Exercise extends App {
  // 4.4
  val l1 = List(Some(1), Some(2), Some(3))
  val l2 = List(Some(1), None, Some(3))

  assert(Option.sequence(l1) == Some(List(1, 2, 3)))
  assert(Option.sequence(l2) == None)

  // 4.5
  def tryOption[A](a: => A): Option[A] =
    try {
      Some(a)
    } catch {
      case _: Exception => None
    }
  val l3 = List("1", "2", "3")
  val l4 = List("1", "a", "3")
  assert(Option.traverse(l3)(s => tryOption(s.toInt)) == Some(List(1, 2, 3)))
  assert(Option.traverse(l4)(s => tryOption(s.toInt)) == None)

  // 4.7
  def tryEither[A](a: => A): Either[Exception, A] =
    try {
      Right(a)
    } catch {
      case e: Exception => Left(e)
    }

  assert(Either.traverse(l3)(s => tryEither(s.toInt)) == Right(List(1, 2, 3)))

  val l5 = List("1", "a", "3", "b")
  Either.traverse(l5)(s => tryEither(s.toInt)) match {
    case Left(e) => {
      tryEither("a".toInt) match {
        case Left(ae) => assert(e.getMessage == ae.getMessage)
        case _ => assert(false)
      }
    }
    case _ => assert(false)
  }
}