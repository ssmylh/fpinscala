package parsing

import scala.util.matching.Regex
import java.util.regex.Pattern

import scala.language.higherKinds
import scala.language.implicitConversions

trait Parsers[Parser[+_]] { self =>
  def run[A](p: Parser[A])(input: String): Either[ParseError, A]
  // Parser[A] -> ParserOps[A]
  implicit def operators[A](p: Parser[A]) = ParserOps[A](p)
  // A -> ParserOps[A] if A => Parser[String] defined
  // and
  // Parser[String] -> ParserOps[String]
  implicit def asStringParser[A](a: A)(implicit f: A => Parser[String]): ParserOps[String] =
    ParserOps(f(a))

  def char(c: Char): Parser[Char] = string(c.toString) map (_.charAt(0))
  // String -> Parser[String]
  implicit def string(s: String): Parser[String]
  // Regex -> Parser[String]
  implicit def regex(r: Regex): Parser[String]
  def slice[A](p: Parser[A]): Parser[String]

  def or[A](p1: Parser[A], p2: => Parser[A]): Parser[A]

  // many1 or succeed(List())
  def many[A](p: Parser[A]): Parser[List[A]] = or(map2(p, many(p))(_ :: _), succeed(List()))
  // 9.8
  def map[A, B](p: Parser[A])(f: A => B): Parser[B] = flatMap(p)(a => succeed(f(a)))
  // In JSON Parser, this succeed implementation causes stack overflow
  //def succeed[A](a: A): Parser[A] = string("") map (_ => a)
  def succeed[A](a: A): Parser[A]

  // 9.1, 9.7
  def map2[A, B, C](p1: Parser[A], p2: => Parser[B])(f: (A, B) => C): Parser[C] = flatMap(p1)(a => map(p2)(b => f(a, b)))
  def product[A, B](p1: Parser[A], p2: => Parser[B]): Parser[(A, B)] = flatMap(p1)(a => map(p2)(b => (a,b)))
  def many1[A](p: Parser[A]): Parser[List[A]] = map2(p, many(p))(_ :: _)

  // 9.4
  def listOfN[A](n: Int, p: Parser[A]): Parser[List[A]] =
    if (n <= 0) succeed(List())
    else map2(p, listOfN(n - 1, p))(_ :: _)

  def flatMap[A, B](p: Parser[A])(f: A => Parser[B]): Parser[B]

  // 9.6
  //def numChars(c: Char): Parser[Int] = "[0-9]+".r flatMap(s => listOfN(s.toInt, char(c)).slice.map(_.size))
  def numChars(c: Char): Parser[Int] = for {
    s <- "[0-9]+".r
    n = s.toInt
    _ <- listOfN(n, char(c))
  } yield n

  def label[A](msg: String)(p: Parser[A]): Parser[A]

  def scope[A](msg: String)(p: Parser[A]): Parser[A]

  def attempt[A](p: Parser[A]): Parser[A]

  // for JSON Parser
  def skipL[A, B](p1: Parser[Any], p2: => Parser[B]): Parser[B] =
    map2(slice(p1), p2)((_, b) => b)
  def skipR[A, B](p1: Parser[A], p2: => Parser[B]): Parser[A] =
    map2(p1, slice(p2))((a, _) => a)
  def surround[A](start: Parser[Any], stop: Parser[Any])(p: => Parser[A]): Parser[A] =
    start *> p <* stop
  def whitespace: Parser[String] = "\\s*".r
  def token[A](p: Parser[A]): Parser[A] =
    attempt(p) <* whitespace
  def through(s: String): Parser[String] =
    (".*?" + Pattern.quote(s)).r// shortest matching
  // recognize string literals like "bar" and throw away first and last double quotation
  def quoted: Parser[String] =
    string("\"") *> through("\"").map(_.dropRight(1))
  def escapedQuoted: Parser[String] =
    token(quoted label "string literal")
  def sep[A](p1: Parser[A], p2: => Parser[Any]): Parser[List[A]] =
    sep1(p1, p2) or succeed(List())
  def sep1[A](p1: Parser[A], p2: => Parser[Any]): Parser[List[A]] =
    map2(p1, many(p2 *> p1))(_ :: _)
  def eof: Parser[String] =
    label("unexpected trailing characters")(regex("\\z".r))
  def root[A](p: Parser[A]): Parser[A] =
    p <* eof
  def doubleString: Parser[String] =
    token("[-+]?([0-9]*\\.)?[0-9]+([eE][-+]?[0-9]+)?".r)
  def double: Parser[Double] =
    doubleString map (_.toDouble) label "double literal"

  case class ParserOps[A](p1: Parser[A]) {
    def |[B >: A](p2: => Parser[B]): Parser[B] = self.or(p1, p2)
    def or[B >: A](p2: => Parser[B]): Parser[B] = self.or(p1, p2)
    def many: Parser[List[A]] = self.many1(p1)
    def map[B](f: A => B): Parser[B] = self.map(p1)(f)
    def map2[B, C](p2: Parser[B])(f: (A, B) => C): Parser[C] = self.map2(p1, p2)(f)
    def product[B](p2: Parser[B]): Parser[(A, B)] = self.product(p1, p2)
    def flatMap[B](f: A => Parser[B]): Parser[B] = self.flatMap(p1)(f)
    def slice: Parser[String] = self.slice(p1)
    def label(msg: String): Parser[A] = self.label(msg)(p1)
    def scope(msg: String): Parser[A] = self.scope(msg)(p1)
    def attempt: Parser[A] = self.attempt(p1)

    // for JSON Parser
    def skipL[B](p2: => Parser[B]) = self.skipL(p1, p2)
    def skipR(p2: => Parser[Any]) = self.skipR(p1, p2)
    def *>[B](p2: => Parser[B]) = self.skipL(p1, p2)
    def <*(p2: => Parser[Any]) = self.skipR(p1, p2)
    def token = self.token(p1)
    def sep(p2: => Parser[Any]): Parser[List[A]] = self.sep(p1, p2)
    // recognize p1, then use b (in place of p1)
    def as[B](b: B): Parser[B] = self.map(self.slice(p1))(_ => b)
  }
}

case class Location(input: String, offset: Int = 0) {
  lazy val line = input.slice(0, offset + 1).count(_ == '\n') + 1
  lazy val col = input.slice(0, offset + 1).lastIndexOf('\n') match {
    case -1 => offset + 1
    case lineStart => offset - lineStart
  }
  def toError(msg: String): ParseError = ParseError(List((this, msg)))
  def adbanceBy(n: Int) = copy(offset = offset + n)

  def currentLine: String =
    if (input.length > 1) input.lines.drop(line - 1).next
    else ""

  def columnCaret = (" " * (col - 1)) + "^"
}

case class ParseError(stack: List[(Location, String)] = List()) {
  def push(loc: Location, msg: String): ParseError = copy(stack = (loc, msg) :: stack)
  def label[A](s: String): ParseError = ParseError(latestLoc.map((_, s)).toList)
  def latestLoc: Option[Location] = latest map (_._1)
  def latest: Option[(Location, String)] = stack.lastOption

  // 9.16
  override def toString =
    if (stack.isEmpty) "no error message"
    else {
      val collapsed = collapseStack(stack)
      val context = collapsed.lastOption.map("\n\n" + _._1.currentLine).getOrElse("") +
          collapsed.lastOption.map("\n" + _._1.columnCaret).getOrElse("")
      collapsed.map { case (loc, msg) => loc.line.toString + "." + loc.col + " " + msg }.mkString("\n") + context
    }

  def collapseStack(stack: List[(Location, String)]): List[(Location, String)] =
    stack.groupBy(_._1).
      mapValues(_.map(_._2).mkString("; ")).
      toList.sortBy(_._1.offset)
}
