package parsing

import ReferenceTypes._
import scala.util.matching.Regex

object ReferenceTypes {
  type Parser[+A] = Location => Result[A]

  sealed trait Result[+A] {
    def mapError(f: ParseError => ParseError): Result[A] = this match {
      case Failure(e, c) => Failure(f(e), c)
      case _ => this
    }

    def uncommit: Result[A] = this match {
      case Failure(e, true) => Failure(e, false)
      case _ => this
    }

    def addCommit(isCommited: Boolean): Result[A] = this match {
      case Failure(e, c) => Failure(e, c || isCommited)
      case _ => this
    }

    def advanceSuccess(n: Int): Result[A] = this match {
      case Success(a, m) => Success(a, n + m)
      case _ => this
    }
  }
  case class Success[+A](get: A, charsConsumed: Int) extends Result[A]
  case class Failure(get: ParseError, isCommited: Boolean) extends Result[Nothing]

  def firstNonmatchingIndex(input: String, word: String, offset: Int): Int = {
    var i = 0
    while (i < input.length && i < word.length) {
      if (input.charAt(i + offset) != word.charAt(i)) return i
      i += 1
    }
    if (input.length - offset >= word.length) -1
    else input.length - offset
  }
}

object Reference extends Parsers[Parser] {
  override def succeed[A](a: A): Parser[A] = s => Success(a, 0)

  def scope[A](msg: String)(p: Parser[A]): Parser[A] =
    loc => p(loc).mapError(_.push(loc, msg))

  def label[A](msg: String)(p: Parser[A]): Parser[A] =
    loc => p(loc).mapError(_.label(msg))

  def attempt[A](p: Parser[A]): Parser[A] =
    loc => p(loc).uncommit

  def or[A](p1: Parser[A], p2: => Parser[A]): Parser[A] =
    loc => p1(loc) match {
      case Failure(e, false) => p2(loc)
      case r => r
    }

  def flatMap[A, B](p: Parser[A])(f: A => Parser[B]): Parser[B] =
    loc => p(loc) match {
      case Success(a, n) => f(a)(loc.adbanceBy(n))
        .addCommit(n != 0)
        .advanceSuccess(n)
      case f @ Failure(_, _) => f
    }

  def run[A](p: Parser[A])(input: String): Either[ParseError, A] = p(Location(input)) match {
    case Success(a, _) => Right(a)
    case Failure(e, _) => Left(e)
  }

  def string(w: String): Parser[String] = loc => {
    val i = firstNonmatchingIndex(loc.input, w, loc.offset)
    if (i == -1)
      Success(w, w.length)
    else
      Failure(loc.adbanceBy(i).toError("'" + w + "'"), i != 0)
  }

  def slice[A](p: Parser[A]): Parser[String] =
    loc => p(loc) match {
      case Success(_, n) => Success(loc.input.slice(loc.offset, loc.offset + n), n)
      case f @ Failure(_, _) => f
    }

  def regex(r: Regex): Parser[String] =
    loc => r.findPrefixOf(loc.input.substring(loc.offset)) match { // pass a substring specifyning offset
      case None => Failure(loc.toError("regex " + r), false) // regex matching is all or nothing
      case Some(s) => Success(s, s.length)
    }
}