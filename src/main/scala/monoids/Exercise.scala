package monoids

import state._
import testing._
import Monoid._

object Exercise extends App {
  // 10.4
  val i = Gen.choose(-100, 100)

  val intAdditionProp = monoidLaws(intAddition)(i)
  Prop.run(intAdditionProp)

  val intMultiplicationProp = monoidLaws(intMultiplication)(i)
  Prop.run(intMultiplicationProp)

  val booleanOrProp = monoidLaws(booleanOr)(Gen.boolean)
  Prop.run(booleanOrProp)

  val booleanAndProp = monoidLaws(booleanAnd)(Gen.boolean)
  Prop.run(booleanAndProp)

  // 10.7
  assert(foldMapV(0 to 100, string)(_.toString) == foldMap((0 to 100).toList, string)(_.toString))

  // 10.9
  assert(sorted(0 to 10))
  assert(sorted(IndexedSeq(3, 1, 2, 4, 5, 6)) == false)

  // 10.11
  val count = countWord("lorem ipsum dolor sit amet, ")
  assert(count == 5)

  // 10.18
  val b = bag(Vector("a", "rose", "is", "a", "rose"))
  assert(b.get("a") == Some(2))
  assert(b.get("rose") == Some(2) )
  assert(b.get("is") == Some(1))
}