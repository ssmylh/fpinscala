package monoids

import testing._

trait Monoid[A] {
  def op(a1: A, a2: A): A
  def zero: A
}

object Monoid {
  // 10.1
  val intAddition: Monoid[Int] = new Monoid[Int] {
    def op(a1: Int, a2: Int): Int = a1 + a2
    def zero = 0
  }
  val intMultiplication: Monoid[Int] = new Monoid[Int] {
    def op(a1: Int, a2: Int): Int = a1 * a2
    def zero = 1;
  }
  val booleanOr: Monoid[Boolean] = new Monoid[Boolean] {
    def op(a1: Boolean, a2: Boolean): Boolean = a1 || a2
    def zero = false
  }
  val booleanAnd: Monoid[Boolean] = new Monoid[Boolean] {
    def op(a1: Boolean, a2: Boolean): Boolean = a1 && a2
    def zero = true
  }
  // 10.2
  def firstOptionMonoid[A]: Monoid[Option[A]] = new Monoid[Option[A]] {
    def op(a1: Option[A], a2: Option[A]): Option[A] = a1.orElse(a2)
    def zero = None
  }
  def lastOptionMonoid[A]: Monoid[Option[A]] = new Monoid[Option[A]] {
    def op(a1: Option[A], a2: Option[A]): Option[A] = a2.orElse(a1)
    def zero = None
  }
  // dual
  // intAddition, intMultiplication, booleanOr and booleanAnd are equivalent to their dual
  // becaouse thier op is commutative.
  def dual[A](m: Monoid[A]): Monoid[A] = new Monoid[A] {
    def op(a1: A, a2: A): A = m.op(a2, a1)
    def zero = m.zero
  }
  // 10.3
  // right-associative composition version.
  def endoMonoid[A]: Monoid[A => A] = new Monoid[A => A] {
    def op(a1: A => A, a2: A => A): A => A = a1 compose a2
    def zero: A => A = identity
  }
  // 10.5
  def foldMap[A, B](as: List[A], m: Monoid[B])(f: A => B): B = as.map(f).foldLeft(m.zero)(m.op)
  // 10.6
  def foldRightViaFoldMap[A, B](as: List[A])(z: B)(f: (A, B) => B): B =
    foldMap(as, endoMonoid[B])(f.curried)(z)
  def foldLeftViaFoldMap[A, B](as: List[A])(z: B)(f: (B, A) => B): B = {
    val flipped: (A, B) => B = (a, b) => f(b, a)
    foldMap(as, dual(endoMonoid[B]))(flipped.curried)(z)
  }

  // 10.4
  def monoidLaws[A](m: Monoid[A])(gen: Gen[A]): Prop = {
    val associativity = Prop.forAll(for {
      x <- gen
      y <- gen
      z <- gen
    } yield (x, y, z))(p =>
      m.op(p._1, m.op(p._2, p._3)) == m.op(m.op(p._1, p._2), p._3))

    val identity = Prop.forAll(gen)((a: A) =>
      m.op(a, m.zero) == a && m.op(m.zero, a) == a)
    associativity && identity
  }

  // 10.7
  val string: Monoid[String] = new Monoid[String] {
    def op(a1: String, a2: String): String = a1 + a2
    def zero = ""
  }
  def foldMapV[A, B](v: IndexedSeq[A], m: Monoid[B])(f: A => B): B = {
    val (v1, v2) = v.splitAt(v.length)
    m.op(v1.map(f).foldLeft(m.zero)(m.op), v2.map(f).foldLeft(m.zero)(m.op))
  }

  // 10.9
  val intAsc: Monoid[Option[(Int, Int, Boolean)]] = new Monoid[Option[(Int, Int, Boolean)]] {
    def op(a1: Option[(Int, Int, Boolean)], a2: Option[(Int, Int, Boolean)]): Option[(Int, Int, Boolean)] = (a1, a2) match {
      case (Some((s1, e1, b1)), Some((s2, e2, b2))) =>
        Some((s1 min s2, e1 max e2, b1 && b2 && (e1 <= s2)))
      case (None, a) => a
      case (a, None) => a
    }
    def zero = None
  }

  def sorted(is: IndexedSeq[Int]): Boolean = {
    foldMap(is.toList, intAsc)(i => Some((i, i, true))).map(_._3).getOrElse(true)
  }

  // 10.10
  sealed trait WC
  case class Stub(chars: String) extends WC
  case class Part(lStub: String, words: Int, rStub: String) extends WC

  val wcMonoid: Monoid[WC] = new Monoid[WC] {
    def op(a1: WC, a2: WC): WC = (a1, a2) match {
      case (Stub(c1), Stub(c2)) => Stub(c1 + c2)
      case (Stub(c), Part(l, w, r)) => Part(c + l, w, r)
      case (Part(l, w, r), Stub(c)) => Part(l, w, r + c)
      case (Part(l1, w1, r1), Part(l2, w2, r2)) => {
        if ((r1 + l2).isEmpty)
          Part(l1, w1 + w2, r2)
        else
          Part(l1, w1 + w2 + 1, r2)
      }
      case (Stub(""), a) => a
      case (a, Stub("")) => a
    }
    def zero = Stub("")
  }

  // 10.11
  def countWord(str: String): Int = {
    def char2WC(c: Char): WC =
      if (c.isWhitespace)
        Part("", 0, "")
      else
        Stub(c.toString)
    def asOne(s: String): Int = if (s.isEmpty) 0 else 1
    foldMapV(str.toIndexedSeq, wcMonoid)(char2WC) match {
      case Stub(c) => asOne(c)
      case Part(l, w, r) => asOne(l) + w + asOne(r)
    }
  }

  // 10.16
  def productMonoid[A, B](A: Monoid[A], B: Monoid[B]): Monoid[(A, B)] = new Monoid[(A, B)] {
    def op(a1: (A, B), a2: (A, B)): (A, B) = (A.op(a1._1, a2._1), B.op(a1._2, a2._2))
    def zero: (A, B) = (A.zero, B.zero)
  }

  // 10.17
  def functionMonoid[A, B](B: Monoid[B]): Monoid[A => B] = new Monoid[A => B] {
    def op(a1: A => B, a2: A => B): A => B =
      a => B.op(a1(a), a2(a))
    def zero: A => B = _ => B.zero
  }

  // 10.18
  def mapMergeMonoid[K, V](V: Monoid[V]): Monoid[Map[K, V]] = new Monoid[Map[K, V]] {
    def zero = Map[K, V]()
    def op(a: Map[K, V], b: Map[K, V]) =
      (a.keySet ++ b.keySet).foldLeft(zero) { (acc, k) =>
        acc.updated(k, V.op(a.getOrElse(k, V.zero), b.getOrElse(k, V.zero)))
      }
  }
  def bag[A](as: IndexedSeq[A]): Map[A, Int] =
    foldMapV(as, mapMergeMonoid[A, Int](intAddition))(a => Map(a -> 1))
}