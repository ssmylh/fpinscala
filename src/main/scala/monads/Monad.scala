package monads

import applicative._
import parsing._
import state._

import scala.language.higherKinds

trait Monad[F[_]] extends Applicative[F] {
  def unit[A](a: => A): F[A]
  // 11. 8
  def flatMap[A, B](fa: F[A])(f: A => F[B]): F[B] =
    compose(identity[F[A]], f)(fa)
  override def map[A, B](fa: F[A])(f: A => B): F[B] =
    flatMap(fa)(a => unit(f(a)))
  override def map2[A, B, C](fa: F[A], fb: F[B])(f: (A, B) => C): F[C] =
    flatMap(fa)(a => map(fb)(b => f(a, b)))

  // 11.3
  override def sequence[A](lfa: List[F[A]]): F[List[A]] = traverse(lfa)(identity)
  override def traverse[A, B](la: List[A])(f: A => F[B]): F[List[B]] =
    la.foldRight(unit(List[B]()))((a, acc) => map2(f(a), acc)(_ :: _))

  // 11.4
  override def replicateM[A](n: Int, fa: F[A]): F[List[A]] = sequence(List.fill(n)(fa))

  // 11.6
  def filterM[A](la: List[A])(f: A => F[Boolean]): F[List[A]] =
    la.foldRight(unit(List[A]()))((a, acc) => map2(f(a), acc) { (b, as) =>
      if (b) a :: as
      else as
    })

  // 11. 7
  def compose[A, B, C](f: A => F[B], g: B => F[C]): A => F[C] =
    a => flatMap(f(a))(g)

  // 11.12
  def join[A](ffa: F[F[A]]): F[A] = flatMap(ffa)(identity)

  // 11.13
  def flatMapViaJoin[A, B](fa: F[A])(f: A => F[B]): F[B] = join(map(fa)(f))

  // 12.11
  def compose[G[_]](mg: Monad[G]): Monad[({ type L[X] = F[G[X]] })#L] = {
    val self = this
    new Monad[({ type L[X] = F[G[X]] })#L] {
      def unit[A](a: => A): F[G[A]] = self.unit(mg.unit(a))
      override def flatMap[A, B](fga: F[G[A]])(f: A => F[G[B]]): F[G[B]] = {
        val r1 = self.flatMap(fga)(ga => {
          val r2 = mg.flatMap(ga)(a => ???)
          ???
        })
        ???
      }
      def exercise[A, B](fga: F[G[A]])(f:A => F[G[B]]): F[G[F[G[B]]]] = {
        // F[G[F[G[B]]]]
        //
        // if F and G are could be swapped,
        //
        // F[F[G[G[B]]]]
        //
        // then G is flattened and also F is flattened,
        //
        // F[G[B]]
        self.map(fga)(ga => mg.map(ga)(f))
      }
    }
  }
}

object Monad {
  // 11.1
  def parserMonad[Parser[+_]](p: Parsers[Parser]) = new Monad[Parser] {
    def unit[A](a: => A): Parser[A] = p.succeed(a)
    override def flatMap[A, B](fa: Parser[A])(f: A => Parser[B]): Parser[B] = p.flatMap(fa)(f)
  }

  val listMonad = new Monad[List] {
    def unit[A](a: => A): List[A] = List(a)
    override def flatMap[A, B](fa: List[A])(f: A => List[B]): List[B] = fa flatMap f
  }

  val streamMonad = new Monad[Stream] {
    def unit[A](a: => A): Stream[A] = Stream(a)
    override def flatMap[A, B](fa: Stream[A])(f: A => Stream[B]): Stream[B] = fa flatMap f
  }

  val optionMonad = new Monad[Option] {
    def unit[A](a: => A): Option[A] = Some(a)
    override def flatMap[A, B](fa: Option[A])(f: A => Option[B]): Option[B] = fa flatMap f
  }

  // 11.2
  def stateMonad[S] = new Monad[({ type F[X] = State[S, X] })#F] {
    def unit[A](a: => A): State[S, A] = State.unit(a)
    override def flatMap[A, B](fa: State[S, A])(f: A => State[S, B]): State[S, B] = fa flatMap f
  }

  // 12.5
  def eitherMonad[E] = new Monad[({ type F[X] = Either[E, X] })#F] {
    def unit[A](a: => A): Either[E, A] = Right(a)
    override def flatMap[A, B](fa: Either[E, A])(f: A => Either[E, B]): Either[E, B] = fa match {
      case Right(a) => f(a)
      case Left(e) => Left(e)
    }
  }

  // 12.20
  // sequence
  //   F[G[A]] => G[F[A]]
  //   F is Traverse
  //   G is Applicative or Monad
  def composeM[F[_], G[_]](F: Monad[F], G: Monad[G], T: Traverse[G]): Monad[({ type L[X] = F[G[X]] })#L] =
    new Monad[({ type L[X] = F[G[X]] })#L] {
      def unit[A](a: => A): F[G[A]] = F.unit(G.unit(a))
      override def flatMap[A, B](fga: F[G[A]])(f: A => F[G[B]]): F[G[B]] = F.flatMap(fga)(ga => {
        val gfgb = G.map(ga)(a => f(a))
        val fggb = T.sequence(gfgb)(F)
        val fgb = F.map(fggb)(ggb => G.join(ggb))
        fgb
      })
    }
}