package monads

import applicative._
import Monad._
import state._, State._
import Reader._

object Exercise extends App {
  // 11.5
  // trace replicateM(3, List(1, 2, 3))
  // '3' of 'r3' is '3' of replicateM(3, l1).
  val l1 = List(1, 2, 3)
  val r1 = listMonad.map2(l1, List(List()))(_ :: _)
  // r1 = List(List(1), List(2), List(3))
  val r2 = listMonad.map2(l1, r1)(_ :: _)
  // r2 = List(List(1, 1), List(1, 2), List(1, 3),
  //           List(2, 1), List(2, 2), List(2, 3),
  //           List(3, 1), List(3, 2), List(3, 3))
  val r3 = listMonad.map2(l1, r2)(_ :: _)
  // r3 = List(List(1, 1, 1), List(1, 1, 2), List(1, 1, 3),
  //           List(1, 2, 1), List(1, 2, 2), List(1, 2, 3),
  //           List(1, 3, 1), List(1, 3, 2), List(1, 3, 3),
  //           List(2, 1, 1), List(2, 1, 2), List(2, 1, 3),
  //           List(2, 2, 1), List(2, 2, 2), List(2, 2, 3),
  //           List(2, 3, 1), List(2, 3, 2), List(2, 3, 3),
  //           List(3, 1, 1), List(3, 1, 2), List(3, 1, 3),
  //           List(3, 2, 1), List(3, 2, 2), List(3, 2, 3),
  //           List(3, 3, 1), List(3, 3, 2), List(3, 3, 3))
  assert(listMonad.replicateM(3, l1) == r3)

  // 11.6
  val r4 = listMonad.filterM(l1)(a => {
    if (a % 2 == 0) List(true, false, true)
    else List(false, false)
  })
  assert(r4 == List(List(2), List(2), List(), List(), List(2), List(2),
    List(2), List(2), List(), List(), List(2), List(2)))

  // 11.10
  //  -- right identity
  //  compose(f, unit) == f
  //  a => flatMap(f(a))(unit) == f
  //  flatMap(f(a))(unit) == f(a)
  //  ∴ flatMap(x)(unit) == x
  //
  //  -- left identity
  //  compose(unit, f) == f
  //  a => flatMap(unit(a))(f) == f
  //  flatMap(unit(a))(f) == f(a)
  //  ∴ flatMap(unit(y))(f) == f(y)

  // 11.11
  //  list
  //
  //  -- right identity
  //  x = list: List[A]
  //
  //  flatMap(list)(unit) =
  //  flatMap(list)(a => List(a)) =
  //  list
  //  ∴ flatMap(x)(unit) == x
  //
  //  -- left identity
  //  y = a: A
  //  f: A => List[A]
  //
  //  flatMap(unit(a))(f) =
  //  flatMap(List(a))(f) =
  //  f(a)
  //  ∴ flatMap(unit(y))(f) == f(y)

  // 11.14
  //  -- associativity
  //  join(map(
  //    join(map(x)(f))
  //  (g))
  //  ==
  //  join(map(x)(a =>
  //    join(map(f(a))(g))
  //  ))
  //
  //  -- right identity
  //  join(map(x)(unit)) == x
  //
  //  -- left identity
  //  join(map(unit(y))(f)) == f(y)
  //
  //  if both f and g are equal to `identity` :
  //  join(map(x)(unit)) == x
  //  join(unit(x) == x
  //  join(join(x)) == join(map(x)(join)) // if type of x is FF[X], `flatten` outer F of x is equal to `flatten` inner F of x.

  // 11.18
  def push(a: Int): State[List[Int], Unit] = State(as => ((), a :: as))
  def pop: State[List[Int], Int] = State {
    case a :: as => (a, as)
    case _ => (Int.MinValue, Nil)
  }
  val stackOperation: State[List[Int], Int] = for {
    _ <- push(4)
    _ <- push(5)
    a <- pop
  } yield a
  assert(stackOperation.run(l1)._1 == 5)

  val sm1 = stateMonad[List[Int]].replicateM(3, push(1))
  assert(sm1.run(l1) == (List((), (), ()), List(1, 1, 1, 1, 2, 3)))

  val sm2 = stateMonad[List[Int]].map2(pop, pop)((i1, i2) => i1 + i2)
  assert(sm2.run(l1) == (3, List(3)))

  val sm3 = stateMonad[List[Int]].sequence(List(pop, pop))
  assert(sm3.run(l1) == (List(1, 2), List(3)))

  // 11.20
  val add: Int => Reader[Int, Int] = i => Reader(r => r + i)
  val multiply: Int => Reader[Int, Int] = i => Reader(r => r * i)
  val rm2 = add(1).flatMap(multiply)
  assert(rm2.run(2) == 6)// `2` * (`2` + 1)

  val rm3 = readerMonad[Int].sequence(List(add(1), add(2), add(3)))
  assert(rm3.run(4) == List(5, 6, 7))// List(`4` + 1, `4` + 2, `4` + 1)

  // pass the `r` to both the outer reader and the inner reader.
  val rm4: Reader[Int, Reader[Int, Int]] = readerMonad[Int].map(add(1))(multiply)
  assert(readerMonad[Int].join(rm4).run(2) == 6)
  assert(rm4.run(2).run(2) == 6)// passing 2 to both the outer reader and the inner reader.

  // 12.20
  val lo = composeM(listMonad, optionMonad, Traverse.optionTraverse)
  assert(lo.map(List(Some(1), None, Some(2)))(_ + 1) == List(Some(2), None, Some(3)))
}