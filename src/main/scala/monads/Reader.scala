package monads

case class Reader[R, A](run: R => A) {
  def flatMap[B](f: A => Reader[R, B]): Reader[R, B] = Reader(r => {
    f(run(r)).run(r)
  })
}
object Reader {
  def readerMonad[R] = new Monad[({ type F[X] = Reader[R, X] })#F] {
    def unit[A](a: => A): Reader[R, A] = Reader(r => a)
    override def flatMap[A, B](ra: Reader[R, A])(f: A => Reader[R, B]): Reader[R, B] =
      ra flatMap f
  }
}