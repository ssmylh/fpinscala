package gettingstarted

object Exercise extends App {
  def fib1(n: Int): Int = n match {
    case x if x <= 0 => 0
    case 1 => 1
    case _ => fib1(n - 2) + fib1(n - 1)
  }

  // 2.1
  def fib2(n: Int): Int = {
    @annotation.tailrec
    def go(n: Int, previous: Int, current: Int): Int =
      if (n == 0) previous
      else go(n - 1, current, previous + current)
    go(n, 0, 1)
  }

  assert(fib1(6) == 8)
  assert(fib2(6) == 8)

  // 2.2
  def isSorted[A](as: Array[A], orderd: (A, A) => Boolean): Boolean = {
    @annotation.tailrec
    def loop(n: Int): Boolean = {
      if (n == as.length - 1) true
      else if (orderd(as(n), as(n + 1))) loop(n + 1)
      else false
    }
    loop(0)
  }

  assert(isSorted(Array(1,2,3), (x: Int, y: Int) => x < y))

  // 2.3
  def curry[A, B, C](f: (A, B) => C): A => (B => C) =
    a => b => f(a, b)

  // 2.4
  def uncurry[A, B, C](f: A => B => C): (A, B) => C =
    (a, b) => f(a)(b)

  // 2.5
  def compose[A, B, C](f: B => C, g: A => B): A => C =
    a => f(g(a))
}