package laziness

object Exercise extends App {
  val s1 = Stream(1, 2, 3, 4, 5)

  // 5.1
  assert(s1.toList == List(1, 2, 3, 4, 5))

  // 5.2
  assert(s1.take(3).toList == List(1, 2, 3))
  assert(s1.drop(3).toList == List(4, 5))

  // 5.3
  val s2 = Stream(1, 2, 3, 1)
  assert(s2.takeWhile(_ < 3).toList == List(1, 2))

  // 5.4
  assert(s1.forAll(_ < 4) == false)

  // 5.5
  assert(s2.takeWhileViaFoldRight(_ < 3).toList == List(1, 2))

  // 5.6
  assert(s1.headOption == Some(1))

  // 5.7
  assert(s1.map(_ * 2).toList == List(2, 4, 6, 8, 10))
  assert(s2.filter(_ < 3).toList == List(1, 2, 1))

  // 5.8
  assert(Stream.constant(1).take(3).toList == List(1, 1, 1))

  // 5.9
  assert(Stream.from(5).take(3).toList == List(5, 6, 7))

  // 5.10
  assert(Stream.fibs().take(7).toList == List(0, 1, 1, 2, 3, 5, 8))

  // 5.12
  assert(Stream.fibsViaUnfold().take(7).toList == List(0, 1, 1, 2, 3, 5, 8))
  assert(Stream.fromViaUnfold(5).take(3).toList == List(5, 6, 7))
  assert(Stream.constantViaUnfold(1).take(3).toList == List(1, 1, 1))

  // 5.13
  assert(s1.mapViaUnfold(_ * 2).toList == List(2, 4, 6, 8, 10))
  assert(s1.takeViaUnfold(3).toList == List(1, 2, 3))
  assert(s2.takeWhileViaUnfold(_ < 3).toList == List(1, 2))
  val s3 = Stream(-1, -2, -3, -4, -5, -6)
  assert(s1.zipWith(s3)(_ + _).toList == List(0, 0, 0, 0, 0))
  val s4 = Stream(2, 3, 4, 5, 6, 7)
  assert(s1.zipAll(s4).toList == List(
    (Some(1), Some(2)),
    (Some(2), Some(3)),
    (Some(3), Some(4)),
    (Some(4), Some(5)),
    (Some(5), Some(6)),
    (None, Some(7))))

  // 5.14
  val s5 = Stream(1, 2, 3)
  assert(s1 startsWith s5)

  // 5.15
  assert(s5.tails.map(_.toList).toList == List(List(1, 2, 3), List(2, 3), List(3), List()))

  // 5.16
  assert(s5.scanRight(0)(_ + _).toList == List(6, 5, 3, 0))
}