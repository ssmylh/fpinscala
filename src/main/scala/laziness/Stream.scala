package laziness

import scala.{ Stream => _ }

trait Stream[+A] {
  import Stream._

  def headOption: Option[A] = this match {
    case Empty => None
    case Cons(h, t) => Some(h())
  }

  // 5.1
  def toList: List[A] = {
    @annotation.tailrec
    def go(s: Stream[A], acc: List[A]): List[A] = s match {
      case Empty => acc
      case Cons(h, t) => go(t(), h() :: acc)
    }
    go(this, Nil).reverse
  }

  // 5.2
  def take(n: Int): Stream[A] = this match {
    case Cons(h, t) if n > 1 => cons(h(), t().take(n - 1))
    case Cons(h, _) if n == 1 => cons(h(), empty)
    case _ => empty
  }
  def drop(n: Int): Stream[A] = this match {
    case Cons(_, t) if n > 0 => t().drop(n - 1)
    case _ => this
  }

  // 5.3
  def takeWhile(p: A => Boolean): Stream[A] = this match {
    case Cons(h, t) if p(h()) => cons(h(), t().takeWhile(p))
    case _ => empty
  }

  def exists(p: A => Boolean): Boolean = this match {
    case Cons(h, t) => p(h()) || t().exists(p)
    case _ => false
  }

  // f may choose not to evaluate second argument.
  def foldRight[B](z: => B)(f: (A, => B) => B): B = this match {
    // if f does not evaluate second argument, recursion does not occur.
    case Cons(h, t) => f(h(), t().foldRight(z)(f))
    case _ => z
  }

  // 5.4
  def forAll(p: A => Boolean): Boolean = foldRight(true)((a, b) => p(a) && b)

  // 5.5
  def takeWhileViaFoldRight(p: A => Boolean): Stream[A] = foldRight(empty[A]) { (a, b) =>
    if (p(a)) cons(a, b)
    else empty
  }

  // 5.6
  def headOptionViaFoldRight: Option[A] = foldRight(None: Option[A])((a, _) => Some(a))

  // 5.7
  // b of cons(a, b) is not evaluated.
  def map[B](f: A => B): Stream[B] = foldRight(empty[B])((a, b) => cons(f(a), b))
  def filter(f: A => Boolean): Stream[A] = foldRight(empty[A]) { (a, b) =>
    if (f(a)) cons(a, b)
    else b
  }
  def append[B >: A](s: => Stream[B]): Stream[B] = foldRight(s)((a, b) => cons(a, b))
  def flatMap[B](f: A => Stream[B]): Stream[B] = foldRight(empty[B])((a, b) => f(a).append(b))

  // 5.13
  def mapViaUnfold[B](f: A => B): Stream[B] = unfold(this) {
    case Cons(h, t) => Some((f(h()), t()))
    case Empty => None
  }
  def takeViaUnfold(n: Int): Stream[A] = unfold((n, this)) {
    case (i, s) if i > 0 => s match {
      case Cons(h, t) => Some((h(), (i - 1, t())))
      case _ => None
    }
    case _ => None
  }
  def takeWhileViaUnfold(p: A => Boolean): Stream[A] = unfold(this) {
    case Cons(h, t) if (p(h())) => Some((h(), t()))
    case _ => None
  }
  def zipWith[B, C](s2: Stream[B])(f: (A, B) => C): Stream[C] = unfold((this, s2)) {
    case (Cons(h1, t1), Cons(h2, t2)) => Some((f(h1(), h2()), (t1(), t2())))
    case _ => None
  }
  def zip[B](s2: Stream[B]): Stream[(A,B)] = zipWith(s2)((_, _))
  def zipAll[B](s2: Stream[B]): Stream[(Option[A], Option[B])] = unfold((this, s2)) {
    case (Cons(h1, t1), Cons(h2, t2)) => Some(((Some(h1()), Some(h2())), (t1(), t2())))
    case (Cons(h1, t1), Empty) => Some(
      (
        (Some(h1()), None), (t1(), empty)))
    case (Empty, Cons(h2, t2)) => Some(
      (
        (None, Some(h2())), (empty, t2())))
    case _ => None
  }

  // 5.14
  def startsWith[A](s: Stream[A]): Boolean = zipWith(s)(_ == _).forAll(identity)

  // 5.15
  // Stream(1,2,3).tals -> Stream(Stream(1,2,3), Stream(2,3), Stream(3), Stream())
  def tails: Stream[Stream[A]] = unfold(this) {
    case s @ Cons(h, t) => Some((s, t()))
    case Empty => None
  } append Stream(empty)

  // 5.16
  // Stream(1,2,3).scanRight(_ + _).toList = List(6,5,3,0)
  //  def scanRight[B](z: => B)(f: (A, => B) => B): Stream[B] = this match {
  //    case Cons(h, t) => cons(f(h(), t().foldRight(z)(f)), t().scanRight(z)(f))
  //    case Empty => cons(z, empty)
  //  }
  def scanRight[B](z: B)(f: (A, => B) => B): Stream[B] =
    foldRight((z, Stream(z)))((a, p0) => {
      lazy val p1 = p0
      val b2 = f(a, p1._1)
      (b2, cons(b2, p1._2))
    })._2

  def find(f: A => Boolean): Option[A] = filter(f).headOption
}
case object Empty extends Stream[Nothing]
case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

object Stream {
  def cons[A](hd: => A, tl: => Stream[A]) = {
    lazy val head = hd
    lazy val tail = tl
    Cons(() => head, () => tail)
  }
  def empty[A]: Stream[A] = Empty
  def apply[A](as: A*): Stream[A] =
    if (as.isEmpty) empty
    else cons(as.head, apply(as.tail: _*))

  // 5.8
  def constant[A](a: A): Stream[A] = cons(a, constant(a))

  // 5.9
  def from(n: Int): Stream[Int] = cons(n, from(n + 1))

  // 5.10
  def fibs(): Stream[Int] = {
    def go(i1: Int, i2: Int): Stream[Int] = {
      cons(i1, go(i2, i1 + i2))
    }
    go(0, 1)
  }

  // 5.11
  def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = f(z) match {
    case Some((a, s)) => cons(a, unfold(s)(f))
    case None => empty
  }

  // 5.12
  def fibsViaUnfold(): Stream[Int] = unfold((0, 1)) { s =>
    Some(s._1, (s._2, s._1 + s._2))
  }
  def fromViaUnfold(n: Int): Stream[Int] = unfold(n)(s => Some((s, s + 1)))
  def constantViaUnfold[A](a: A): Stream[A] = unfold(a)(s => Some((s, s)))
}
