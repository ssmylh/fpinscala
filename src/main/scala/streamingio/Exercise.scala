package streamingio

object Exercise extends App {
  {
    import Simple._, Process._

    val p1 = liftOne((x: Int) => x * 2)
    val xs = p1(Stream(1, 2, 3)).toList
    assert(xs == List(2))

    // `Emit(1).repeat` causes SOE.
    // Because when the state is `Halt`, the next state is `Emit(1)` (repeats infinitely).

    val even = filter((x: Int) => x % 2 == 0)
    val evens = even(Stream(1, 2, 3, 4)).toList
    assert(evens == List(2, 4))

    val l1 = sum(Stream(1.0, 2.0, 3.0, 4.0)).toList
    assert(l1 == List(1.0, 3.0, 6.0, 10.0))

    // 15.1
    val s1 = Stream(1, 2, 3, 1, 2, 3)
    assert(take(2)(s1).toList == List(1, 2))
    assert(drop(3)(s1).toList == List(1, 2, 3))
    assert(takeWhile((x: Int) => x < 3)(s1).toList == List(1, 2))
    assert(dropWhile((x: Int) => x < 3)(s1).toList == List(3, 1, 2, 3))

    // 15.2
    assert(count(s1).toList == List(1, 2, 3, 4, 5, 6))

    // 15.3
    assert(mean(Stream(1.0, 2.0, 3.0)).toList == List(1.0, 1.5, 2.0))

    // 15.4
    assert(sumViaLoop(Stream(1.0, 2.0, 3.0, 4.0)).toList == List(1.0, 3.0, 6.0, 10.0))
    assert(countViaLoop(s1).toList == List(1, 2, 3, 4, 5, 6))

    // 15.5
    def countViaPipe[I]: Process[I, Int] = lift((i: I) => 1.0) |> sum |> lift(_.toInt)
    assert(countViaPipe(s1).toList == List(1, 2, 3, 4, 5, 6))

    // 15.6
    assert(take(4).zipWithIndex(s1).toList == List((1, 0), (2, 1), (3, 2), (1, 3)))

    // 15.7
    assert(meanViaZip(Stream(1.0, 2.0, 3.0)).toList == List(1.0, 1.5, 2.0))

    // 15.8
    val s2 = Stream(1, 3, 5, 6, 7)
    assert(existsAll[Int](_ % 2 == 0)(s2).toList == List(false, false, false, true, false))
    assert(exists[Int](_ % 2 == 0)(Stream(1, 2, 3)).toList == List(true))
    assert(exists[Int](_ % 2 == 0)(Stream(1, 1, 1)).toList == List(false))

    // 15.9
    {
      import iomonad.IO2._
      val srcPath = Exercise.getClass.getClassLoader.getResource("streamingio/data/fahrenheit.txt").getPath
      val destPath = "/Users/ssmylh/works/scala/celcius.txt"
      val io: IO[Int] = processReadWrite(srcPath, destPath, fahrenheit2Celcius)
      //assert(run(io) == 10)
    }
  }

  {
    import Extensible._, Process._
    import iomonad.IO2._
    import MonadCatch._
    import java.io.{ BufferedReader, FileReader }

    // 15.10
    val srcPath = Exercise.getClass.getClassLoader.getResource("streamingio/data/lines.txt").getPath
    val p: Process[IO, String] = await(IO(new BufferedReader(new FileReader(srcPath)))) {
      case Right(b) =>
        lazy val next: Process[IO, String] = await(IO(b.readLine)) {
          case Left(e) => await(IO(b.close))(_ => Halt(e))
          case Right(line) =>
            if (line eq null) Halt(End)
            else Emit(line, next)
        }
        next
      case Left(e) => Halt(e)
    }

    val io: IO[IndexedSeq[String]] = p.runLog
    assert(run(io).filter(s => s.nonEmpty) == IndexedSeq("a1", "b1", "a2", "b2", "a3", "b3"))
  }

  {
    // 15.11
    import Extensible._, Process._
    import iomonad.IO2._
    val srcPath = Exercise.getClass.getClassLoader.getResource("streamingio/data/lines.txt").getPath
    val p = lines(srcPath)
    val io: IO[IndexedSeq[String]] = p.runLog
    assert(run(io).filter(s => s.nonEmpty) == IndexedSeq("a1", "b1", "a2", "b2", "a3", "b3"))
  }

  {
    // List 15-29
    import Extensible._, Process._
    import iomonad.IO2._
    val srcPath = Exercise.getClass.getClassLoader.getResource("streamingio/data/lines.txt").getPath
    val p = lines(srcPath)
    val io: IO[IndexedSeq[String]] = p.filter(_.startsWith("a")).take(2).runLog
    assert(run(io) == IndexedSeq("a1", "a2"))
  }

  {
    // List 15-33, 34
    import Extensible._, Process._
    import iomonad.IO2._
    def indexes(start: Int): Process[IO, Int] = {
      def go(s: Int): Process[IO, Int] = await(IO(s)) {
        case Right(i) => emit(i, go(i + 1))
        case Left(e) => Halt(e)
      }
      go(start)
    }
    val srcPath = Exercise.getClass.getClassLoader.getResource("streamingio/data/lines.txt").getPath
    val p = indexes(1).tee(lines(srcPath))(zip)
    val io: IO[IndexedSeq[(Int, String)]] = p.take(3).runLog
    assert(run(io) == IndexedSeq(1 -> "a1", 2 -> "b1", 3 -> "a2"))
  }

  {
    // 15.12
    import Extensible._, Process._
    import iomonad.IO2._
    val srcPath = Exercise.getClass.getClassLoader.getResource("streamingio/data/lines.txt").getPath
    val destPath = "/Users/ssmylh/works/scala/lines.txt"
    val to: Process[IO, Unit] =
      lines(srcPath).
      map(_ + "\n").
      to(fileW(destPath)).
      drain
    //run(to.runLog)
  }
}