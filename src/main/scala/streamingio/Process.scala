package streamingio

object Simple {

  sealed trait Process[I, O] {
    import Process._

    def apply(s: Stream[I]): Stream[O] = this match {
      case Halt() => Stream()
      case Await(recv) => s match {
        case h #:: t => recv(Some(h))(t)
        case xs => recv(None)(xs)
      }
      case Emit(h, t) => h #:: t(s)
    }

    def repeat: Process[I, O] = {
      def go(p: Process[I, O]): Process[I, O] = p match {
        case Halt() => go(this)
        case Await(recv) => Await {
          case None => recv(None) // don not repeat when terminated by 'source' !
          case i => go(recv(i))
        }
        case Emit(h, t) => Emit(h, go(t))
      }
      go(this)
    }

    // 15.5
    def |>[O2](p2: Process[O, O2]): Process[I, O2] = p2 match {
      case Halt() => Halt()
      case Emit(h, t) => Emit(h, this |> t)
      case Await(r2) => this match {
        case Halt() => Halt() |> r2(None)
        case Emit(h, t) => t |> r2(Some(h))
        case Await(r) => Await(i => r(i) |> p2)
      }
    }

    def map[O2](f: O => O2): Process[I, O2] = this |> lift(f)

    def ++(p2: => Process[I, O]): Process[I, O] = this match {
      case Halt() => p2
      case Emit(h, t) => Emit(h, t ++ p2)
      case Await(recv) => Await(recv andThen (_ ++ p2))
    }

    def flatMap[O2](f: O => Process[I, O2]): Process[I, O2] = this match {
      case Halt() => Halt()
      case Emit(h, t) => f(h) ++ t.flatMap(f)
      case Await(recv) => Await(recv andThen (_ flatMap f))
    }

    // 15.6
    //  def zipWithIndex: Process[I, (O, Int)] = {
    //    def go(p: Process[I, O], i: Int): Process[I, (O, Int)] = p match {
    //      case Halt() => Halt()
    //      case Emit(h, t) => Emit((h, i), go(t, i + 1))
    //      case Await(recv) => Await(recv andThen (_p => go(_p, i)))
    //    }
    //    go(this, 0)
    //  }
    def zipWithIndex: Process[I, (O, Int)] = zip(this, count map (_ - 1))
  }

  case class Emit[I, O](head: O, tail: Process[I, O] = Halt[I, O]) extends Process[I, O]
  case class Await[I, O](recv: Option[I] => Process[I, O]) extends Process[I, O]
  case class Halt[I, O]() extends Process[I, O]

  object Process {
    def liftOne[I, O](f: I => O): Process[I, O] = Await {
      case Some(i) => Emit(f(i))
      case None => Halt()
    }

    def lift[I, O](f: I => O): Process[I, O] = liftOne(f).repeat

    def filter[I](p: I => Boolean): Process[I, I] = Await[I, I] {
      case Some(i) if p(i) => Emit(i)
      case _ => Halt()
    }.repeat

    def sum: Process[Double, Double] = {
      def go(acc: Double): Process[Double, Double] = Await {
        case Some(d) => Emit(d + acc, go(d + acc))
        case None => Halt()
      }
      go(0.0)
    }

    // helper function
    def await[I, O](f: I => Process[I, O], fallback: Process[I, O] = Halt[I, O]()): Process[I, O] =
      Await[I, O] {
        case Some(i) => f(i)
        case None => fallback
      }
    def id[I]: Process[I, I] = lift(identity)

    // 15.1
    def take[I](n: Int): Process[I, I] =
      if (n <= 0) Halt()
      else await(i => Emit(i, take(n - 1)))
    def drop[I](n: Int): Process[I, I] =
      if (n <= 0) await(i => Emit(i, id))
      else await(i => drop(n - 1))
    def takeWhile[I](f: I => Boolean): Process[I, I] = await(i => {
      if (f(i)) Emit(i, takeWhile(f))
      else Halt()
    })
    def dropWhile[I](f: I => Boolean): Process[I, I] = await(i => {
      if (f(i)) dropWhile(f)
      else Emit(i, id)
    })

    // 15.2
    def count[I]: Process[I, Int] = {
      def go(acc: Int): Process[I, Int] = await(i => Emit(acc, go(acc + 1)))
      go(1)
    }

    // 15.3
    def mean: Process[Double, Double] = {
      def go(sum: Double, count: Int): Process[Double, Double] = await(i => {
        val s = sum + i
        val c = count + 1
        Emit(s / c, go(s, c))
      })
      go(0, 0)
    }

    def loop[S, I, O](z: S)(f: (I, S) => (O, S)): Process[I, O] = await(i => f(i, z) match {
      case (o, s) => Emit(o, loop(s)(f))
    })

    // 15.4
    def sumViaLoop: Process[Double, Double] = loop(0.0)((i, s) => (i + s, i + s))
    def countViaLoop[I]: Process[I, Int] = loop(0)((_, s) => (s + 1, s + 1))

    // 15.7
    def zip[I, O1, O2](p1: Process[I, O1], p2: Process[I, O2]): Process[I, (O1, O2)] = {
      def consume[O](i: Option[I], p: Process[I, O]): Process[I, O] = p match {
        case Await(recv) => recv(i)
        case _ => p
      }
      (p1, p2) match {
        case (Halt(), _) => Halt()
        case (_, Halt()) => Halt()
        case (Emit(h1, t1), Emit(h2, t2)) => Emit((h1, h2), zip(t1, t2))
        case (Await(recv), _) => Await { (i: Option[I]) => zip(recv(i), consume(i, p2)) }
        case (_, Await(recv)) => Await { (i: Option[I]) => zip(consume(i, p1), recv(i)) }
      }
    }

    def meanViaZip: Process[Double, Double] =
      zip(sum, count[Double]) map { case (s, c) => s / c }

    // 15.8
    def existsAll[I](f: I => Boolean): Process[I, Boolean] = loop(false)((i, s) => (f(i) || s, false))
    def exists[I](f: I => Boolean): Process[I, Boolean] =
      existsAll(f) |> dropWhile(!_) |> Await {
        case Some(i) => Emit(i, Halt())
        case _ => Emit(false, Halt())
      }

    // 15.9
    def toCelsius(fahrenheit: Double): Double = (5.0 / 9.0) * (fahrenheit - 32.0)
    def fahrenheit2Celcius: Process[String, String] =
      filter((l: String) => l.trim.nonEmpty) |>
        filter((l: String) => !l.startsWith("#")) |>
        lift((l: String) => toCelsius(l.toDouble).toString)

    import iomonad.IO2._
    def processReadWrite(srcPath: String, destPath: String, process: Process[String, String]): IO[Int] = IO {
      @annotation.tailrec
      def go(src: Iterator[String], out: java.io.PrintWriter, p: Process[String, String], acc: Int): Int = p match {
        case Halt() => acc
        case Emit(h, t) => {
          if (acc != 0) out.println()
          out.write(h)
          go(src, out, t, acc + 1)
        }
        case Await(recv) => {
          val next =
            if (src.hasNext) recv(Some(src.next))
            else recv(None)
          go(src, out, next, acc)
        }
      }

      val src = io.Source.fromFile(srcPath)
      try {
        val out = new java.io.PrintWriter(destPath)
        try {
          go(src.getLines, out, process, 0)
        } finally {
          out.close()
        }
      } finally {
        src.close()
      }
    }
  }

}