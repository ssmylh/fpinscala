package streamingio

import scala.language.higherKinds

object Extensible {

  trait Process[F[_], O] {
    import Process._

    def onHalt(f: Throwable => Process[F, O]): Process[F, O] = this match {
      case Halt(e) => Try(f(e))// convert `e` to `f(e)` to access to the reason for termination.
      case Emit(h, t) => Emit(h, t.onHalt(f))
      case Await(req, recv) => Await(req, recv andThen (_.onHalt(f)))
    }

    def map[O2](f: O => O2): Process[F, O2] = this match {
      case Halt(e) => Halt(e)
      case Emit(h, t) => Try { Emit(f(h), t map f) }
      case Await(req, recv) => Await(req, recv andThen (_ map f))
    }

    def ++(p: => Process[F, O]): Process[F, O] = this.onHalt {
      case End => Try(p)
      case err => Halt(err)
    }

    def onComplete(p: => Process[F, O]): Process[F, O] = this.onHalt {
      case End => p.asFinalizer
      case err => p.asFinalizer ++ Halt(err)
    }

    def asFinalizer: Process[F, O] = this match {
      case halt @ Halt(_) => halt
      case Emit(h, t) => Emit(h, t.asFinalizer)
      case Await(req, recv) => await(req) {
        case Left(Kill) => this.asFinalizer
        case et => recv(et)
      }
    }

    def flatMap[O2](f: O => Process[F, O2]): Process[F, O2] = this match {
      case Halt(err) => Halt(err)
      case Emit(h, t) => Try(f(h)) ++ t.flatMap(f)
      case Await(req, recv) => Await(req, recv andThen (_ flatMap f))
    }

    def repeat: Process[F, O] = this ++ this.repeat

    // 15.10
    def runLog(implicit F: MonadCatch[F]): F[IndexedSeq[O]] = {
      def loop(cur: Process[F, O], acc: IndexedSeq[O]): F[IndexedSeq[O]] = {
        cur match {
          case Emit(h, t) => loop(t, acc :+ h)
          case Halt(End) => F.unit(acc)
          case Halt(err) => F.fail(err)
          case Await(req, recv) => {
            val fe = F.attempt(req)
            F.flatMap(fe)(e => loop(Try(recv(e)), acc)) // need to use `Try` because `F` is not Process.
          }
        }
      }
      loop(this, IndexedSeq())
    }

    def |>[O2](p2: Process1[O, O2]): Process[F, O2] = p2 match {
      case Halt(e) => this.kill.onHalt { Halt(e) ++ Halt(_) } // before halting, gracefully kill `this`.
      case Emit(h, t) => Emit(h, this |> t)
      case Await(req, recv) => this match {
        case Halt(err) => Halt(err) |> recv(Left(err)) // do the cleanup.
        case Emit(h, t) => t |> Try(recv(Right(h)))
        case Await(req0, recv0) => await(req0)(recv0 andThen (_ |> p2))
      }
    }

    /* pass `Kill` to the outer-most `Await`, but ignores remaining outputs.  */
    @annotation.tailrec
    final def kill[O2]: Process[F, O2] = this match {// To be optimized, this method needs to be `final`.
      case Halt(e) => Halt(e)
      case Emit(h, t) => t.kill
      case Await(req, recv) => recv(Left(Kill)).drain.onHalt {
        case Kill => Halt(End)
        case e => Halt(e)
      }
    }

    /* drains all outputs. */
    def drain[O2]: Process[F, O2] = this match {
      case Halt(e) => Halt(e)
      case Emit(h, t) => t.drain
      case Await(req, recv) => Await(req, recv andThen (_.drain))
    }

    def filter(f: O => Boolean): Process[F, O] = this |> Process.filter(f)

    def take(n: Int): Process[F, O] = this |> Process.take(n)

    def tee[O2, O3](p2: Process[F, O2])(t: Tee[O, O2, O3]): Process[F, O3] = t match {
      case Halt(e) => this.kill onComplete p2.kill onComplete Halt(e)
      case Emit(h, t) => Emit(h, (this tee p2)(t))
      case Await(side, recv) => side.get match {
        case Left(isO) => this match {
          case Halt(e) => p2.kill onComplete Halt(e)
          case Emit(o, ot) => (ot tee p2)(Try(recv(Right(o))))
          case Await(reqL, recvL) =>
            await(reqL)(recvL andThen (this2 => this2.tee(p2)(t)))
        }
        case Right(isO2) => p2 match {
          case Halt(e) => this.kill onComplete Halt(e)
          case Emit(o2, ot) => (this tee ot)(Try(recv(Right(o2))))
          case Await(reqR, recvR) =>
            await(reqR)(recvR andThen (p3 => this.tee(p3)(t)))
        }
      }
    }

    def zipWith[O2, O3](p2: Process[F, O2])(f: (O, O2) => O3): Process[F, O3] =
      (this tee p2)(Process.zipWith(f))

    def zip[O2](p2: Process[F, O2]): Process[F, (O, O2)] = zipWith(p2)((_, _))

    def to[O2](sink: Sink[F, O]): Process[F, Unit] =
      join { (this zipWith sink)((o, f) => f(o)) }
  }

  object Process {
    case class Await[F[_], A, O](req: F[A], recv: Either[Throwable, A] => Process[F, O]) extends Process[F, O]
    case class Emit[F[_], O](head: O, tail: Process[F, O]) extends Process[F, O]
    case class Halt[F[_], O](err: Throwable) extends Process[F, O]

    case object End extends Exception
    case object Kill extends Exception

    def Try[F[_], O](p: => Process[F, O]): Process[F, O] =
      try p
      catch { case e: Throwable => Halt(e) }

    def await[F[_], A, O](req: F[A])(recv: Either[Throwable, A] => Process[F, O]): Process[F, O] =
      Await(req, recv)

    def emit[F[_], O](head: O, tail: Process[F, O] = Halt[F, O](End)): Process[F, O] =
      Emit(head, tail)

    // 15.11
    def eval[F[_], A](fa: F[A]): Process[F, A] = await(fa) {
      case Left(e) => Halt(e)
      case Right(a) => Emit(a, Halt(End))
    }
    def eval_[F[_], A, B](fa: F[A]): Process[F, B] = await(fa) {
      case Left(e) => Halt(e)
      case Right(_) => Halt(End)
    }

    import iomonad.IO2._
    def lines(path: String): Process[IO, String] = resource { IO(io.Source.fromFile(path)) } { src =>
      {
        val iter = src.getLines
        def step(ss: Iterator[String]): Option[String] =
          if (ss.hasNext) Some(ss.next)
          else None

        def _lines: Process[IO, String] = eval(IO(step(iter))).flatMap {
          case None => Halt(End)
          case Some(line) => Emit(line, _lines)
        }
        _lines
      }
    } { src => eval_ { IO { src.close } } }

    def resource[R, O](acquire: IO[R])(use: R => Process[IO, O])(release: R => Process[IO, O]): Process[IO, O] =
      eval(acquire) flatMap { r => use(r).onComplete(release(r)) }

    case class Is[I]() {
      sealed trait f[X]
      val Get = new f[I] {}
    }

    def Get[I] = Is[I]().Get
    type Process1[I, O] = Process[Is[I]#f, O]

    def await1[I, O](recv: I => Process1[I, O], fallback: Process1[I, O] = halt1[I, O]): Process1[I, O] =
      Await(Get[I], (e: Either[Throwable, I]) => e match {
        case Left(End) => fallback
        case Left(err) => Halt(err)
        case Right(i) => Try(recv(i))
      })
    def emit1[I, O](head: O, tail: Process1[I, O] = halt1[I, O]): Process1[I, O] = Emit(head, tail)
    def halt1[I, O]: Process1[I, O] = Halt[Is[I]#f, O](End)

    def lift[I, O](f: I => O): Process1[I, O] =
      await1[I, O](i => emit(f(i))).repeat

    def filter[I](f: I => Boolean): Process1[I, I] =
      await1[I, I](i => if (f(i)) emit1(i) else halt1).repeat

    def take[I](n: Int): Process1[I, I] =
      if (n <= 0) halt1
      else await1[I, I](i => emit1(i, take(n - 1)))

    case class T[I1, I2]() {
      sealed trait f[X] { def get: Either[I1 => X, I2 => X] }
      val L = new f[I1] { def get = Left(identity) }
      val R = new f[I2] { def get = Right(identity) }
    }
    def L[I1, I2] = T[I1, I2]().L
    def R[I1, I2] = T[I1, I2]().R

    type Tee[I1, I2, O] = Process[T[I1, I2]#f, O]

    def awaitL[I1, I2, O](recv: I1 => Tee[I1, I2, O], fallback: => Tee[I1, I2, O] = haltT[I1, I2, O]): Tee[I1, I2, O] =
      await[T[I1, I2]#f, I1, O](L) {
        case Left(End) => fallback
        case Left(err) => Halt(err)
        case Right(i1) => Try(recv(i1))
      }
    def awaitR[I1, I2, O](recv: I2 => Tee[I1, I2, O], fallback: => Tee[I1, I2, O] = haltT[I1, I2, O]): Tee[I1, I2, O] =
      await[T[I1, I2]#f, I2, O](R) {
        case Left(End) => fallback
        case Left(err) => Halt(err)
        case Right(i2) => Try(recv(i2))
      }
    def emitT[I1, I2, O](head: O, tail: Tee[I1, I2, O] = haltT[I1, I2, O]): Tee[I1, I2, O] = emit(head, tail)
    def haltT[I1, I2, O]: Tee[I1, I2, O] = Halt[T[I1, I2]#f, O](End)

    def zipWith[I1, I2, O](f: (I1, I2) => O): Tee[I1, I2, O] =
      awaitL[I1, I2, O](i1 => awaitR(i2 => emitT(f(i1, i2)))).repeat
    def zip[I1, I2]: Tee[I1, I2, (I1, I2)] = zipWith((_, _))

    type Sink[F[_], O] = Process[F, O => Process[F, Unit]]

    import java.io._
    def fileW(file: String, append: Boolean = false): Sink[IO, String] =
      resource[FileWriter, String => Process[IO, Unit]] { IO { new FileWriter(file, append) } } { w =>
        constant { (s: String) => eval[IO, Unit](IO(w.write(s))) }
      } { w => eval_(IO(w.close)) }
    def constant[A](a: A): Process[IO, A] = eval[IO, A](IO(a)).repeat

    // 15.12
    def join[F[_], O](p: Process[F, Process[F, O]]): Process[F, O] = p.flatMap(identity)

  }

}