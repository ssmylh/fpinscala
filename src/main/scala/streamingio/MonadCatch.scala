package streamingio

import iomonad.Monad

import scala.language.higherKinds

trait MonadCatch[F[_]] extends Monad[F] {
  def attempt[A](fa: F[A]): F[Either[Throwable, A]]
  def fail[A](t: Throwable): F[A]
}

object MonadCatch {
  import iomonad.IO2._
  implicit val ioCatch: MonadCatch[IO] = new MonadCatch[IO] {
    def unit[A](a: => A): IO[A] = IO.unit(a)
    def flatMap[A, B](fa: IO[A])(f: A => IO[B]): IO[B] = IO.flatMap(fa)(f)
    def attempt[A](fa: IO[A]): IO[Either[Throwable, A]] = IO {
      try Right(run(fa))
      catch { case e: Throwable => Left(e)}
    }
    def fail[A](t: Throwable): IO[A] = throw t
  }
}