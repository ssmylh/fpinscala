package applicative

import monads._

import scala.language.higherKinds

trait Applicative[F[_]] extends Functor[F] {
  // 12.2
  def apply[A, B](fab: F[A => B])(fa: F[A]): F[B] =
    map2(fab, fa)((ab, a) => ab(a))
  def map2[A, B, C](fa: F[A], fb: F[B])(f: (A, B) => C): F[C] =
    apply(apply(unit(f.curried))(fa))(fb)
  def unit[A](a: => A): F[A]

  // 12.2
  def map[A, B](fa: F[A])(f: A => B): F[B] = apply(unit(f))(fa)

  def traverse[A, B](as: List[A])(f: A => F[B]): F[List[B]] =
    as.foldRight(unit(List[B]()))((a, acc) => map2(f(a), acc)(_ :: _))

  // 12.1
  def sequence[A](fas: List[F[A]]): F[List[A]] = traverse(fas)(identity)
  def replicateM[A](n: Int, fa: F[A]): F[List[A]] = sequence(List.fill(n)(fa))
  def product[A, B](fa: F[A], fb: F[B]): F[(A, B)] = map2(fa, fb)((_, _))

  // 12.3
  def map3[A, B, C, D](fa: F[A], fb: F[B], fc: F[C])(f: (A, B, C) => D): F[D] =
    apply(
      apply(
        apply(unit(f.curried))(fa))(fb))(fc)
  def map4[A, B, C, D, E](fa: F[A], fb: F[B], fc: F[C], fd: F[D])(f: (A, B, C, D) => E): F[E] =
    apply(
      apply(
        apply(
          apply(unit(f.curried))(fa))(fb))(fc))(fd)

  // ※6 implement map2 using product and map
  def map2ViaProductAndMap[A, B, C, D](fa: F[A], fb: F[B])(f: (A, B) => C): F[C] =
    map(product(fa, fb))(f.tupled)

  // 12.8
  def product[G[_]](ag: Applicative[G]): Applicative[({ type L[X] = (F[X], G[X]) })#L] = {
    type L[A] = (F[A], G[A])
    val self = this
    new Applicative[L] {
      def unit[A](a: => A): L[A] = (self.unit(a), ag.unit(a))
      override def map2[A, B, C](la: L[A], lb: L[B])(f: (A, B) => C): L[C] = {
        val fc = self.map2(la._1, lb._1)(f)
        val gc = ag.map2(la._2, lb._2)(f)
        (fc, gc)
      }
    }
  }

  // 12.9
  def compose[G[_]](ag: Applicative[G]): Applicative[({ type L[X] = F[G[X]] })#L] = {
    type L[A] = F[G[A]]
    val self = this
    new Applicative[L] {
      def unit[A](a: => A): L[A] = self.unit(ag.unit(a))
      override def map2[A, B, C](la: L[A], lb: L[B])(f: (A, B) => C): L[C] =
        self.map2(la, lb)((ga, gb) => ag.map2(ga, gb)(f))
    }
  }

  // 12.12
  def sequenceMap[K, V](map: Map[K, F[V]]): F[Map[K, V]] =
    map.foldRight(unit(Map.empty[K, V])) { case ((k, fv), acc) => map2(fv, acc)((v, m) => m + (k -> v)) }
}

object Applicative {
  val streamApplicative = new Applicative[Stream] {
    def unit[A](a: => A): Stream[A] = Stream.continually(a)
    override def map2[A, B, C](as: Stream[A], bs: Stream[B])(f: (A, B) => C): Stream[C] =
      as zip bs map f.tupled
  }
  // 12.14
  val idApplicative = new Applicative[Id] {
    def unit[A](a: => A): Id[A] = Id(a)
    override def map2[A, B, C](ida: Id[A], idb: Id[B])(f: (A, B) => C): Id[C] = Id.idMonad.map2(ida, idb)(f)
  }
}
