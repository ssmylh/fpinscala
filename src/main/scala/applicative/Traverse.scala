package applicative

import monads._
import state._, State._

import scala.language.higherKinds

trait Traverse[F[_]] extends Functor[F] {
  def traverse[G[_]: Applicative, A, B](fa: F[A])(f: A => G[B]): G[F[B]] =
    sequence(map(fa)(f))
  def sequence[G[_]: Applicative, A](fga: F[G[A]]): G[F[A]] =
    traverse(fga)(identity)
  // 12.14
  def map[A, B](fa: F[A])(f: A => B): F[B] = {
    // select Id as a `G`.
    val idfb = traverse(fa)(a => Id(f(a)))(Applicative.idApplicative)// satisfy context bound `G[_]: Applicative`.
    idfb.value
  }

  def traverseS[S, A, B](fa: F[A])(f: A => State[S, B]): State[S, F[B]] =
    traverse[({ type L[X] = State[S, X] })#L, A, B](fa)(f)(Monad.stateMonad)

//  def zipWithIndex[A](fa: F[A]): F[(A, Int)] = traverseS(fa)((a: A) => (for {
//    i <- get[Int]
//    _ <- set(i + 1)
//  } yield (a, i))).run(0)._1
//
//  def toList[A](fa: F[A]): List[A] = traverseS(fa)((a: A) => (for {
//    as <- get[List[A]]
//    _ <- set(a :: as)
//  } yield ())).run(Nil)._2.reverse

  def mapAccum[S, A, B](fa: F[A], s: S)(f: (A, S) => (B, S)): (F[B], S) =
    traverseS(fa)((a: A) => (for {
      s1 <- get[S]
      (b, s2) = f(a, s1)
      _ <- set(s2)
    } yield b)).run(s)

  def zipWithIndex[A](fa: F[A]): F[(A, Int)] =
    mapAccum(fa, 0)((a, s) => ((a, s), s + 1))._1

  def toList[A](fa: F[A]): List[A] =
    mapAccum(fa, List[A]())((a, s) => ((), a :: s))._2.reverse

  // 12.16
  def reverse[A](fa: F[A]): F[A] = {
    def toReversedList(fa: F[A]): List[A] = mapAccum(fa, List[A]())((a, s) => ((), a :: s))._2
    // use reversed list as initial state, not use each of fa.
    mapAccum(fa, toReversedList(fa))((_, s) => (s.head, s.tail))._1
  }

  // 12.17
  def foldLeft[A, B](fa: F[A])(z: B)(f: (B, A) => B): B =
    mapAccum(fa, z)((a, b) => ((), f(b, a)))._2

  // 12.18
  def fuse[G[_], H[_], A, B](fa: F[A])(f: A => G[B], g: A => H[B])(G: Applicative[G], H: Applicative[H]): (G[F[B]], H[F[B]]) =
    traverse[({ type L[X] = (G[X], H[X]) })#L, A, B](fa)(a => (f(a), g(a)))(G.product(H))

  // 12.19
  def compose[G[_]](implicit G: Traverse[G]): Traverse[({ type L[X] = F[G[X]] })#L] = {
    val self = this
    new Traverse[({ type L[X] = F[G[X]] })#L] {
      override def traverse[C[_]: Applicative, A, B](fga: F[G[A]])(f: A => C[B]): C[F[G[B]]] =
        self.traverse(fga)(ga =>
          G.traverse(ga)(a => f(a))// return C[G[B]]
        )
    }
  }
}

case class Tree[+A](head: A, tail: List[Tree[A]])

object Traverse {
  // 12.13
  val listTraverse: Traverse[List] = new Traverse[List] {
    override def traverse[G[_], A, B](fa: List[A])(f: A => G[B])(implicit ag: Applicative[G]): G[List[B]] =
      fa.foldRight(ag.unit(List[B]()))((a, acc) => ag.map2(f(a), acc)(_ :: _))
  }
  val optionTraverse: Traverse[Option] = new Traverse[Option] {
    override def traverse[G[_], A, B](fa: Option[A])(f: A => G[B])(implicit ag: Applicative[G]): G[Option[B]] = fa match {
      case Some(a) => ag.map(f(a))(b => Some(b))
      case None => ag.unit(None)
    }
  }
  val treeTraverse = new Traverse[Tree] {
    override def traverse[G[_], A, B](fa: Tree[A])(f: A => G[B])(implicit ag: Applicative[G]): G[Tree[B]] = fa match {
      case Tree(h, tail) => {
        val gb: G[B] = f(h)
        val gfbs: G[List[Tree[B]]] = listTraverse.traverse(tail)(t => traverse(t)(f))
        ag.map2(gb, gfbs)(Tree.apply)
      }
    }
  }
}