package applicative

import Applicative._
import Traverse._
import Validation._
import java.util.Date

object Exercise extends App {
  // 12.4
  val ss = List(Stream(1, 11), Stream(2, 22), Stream(3, 33))
  assert(streamApplicative.sequence(ss).toList == List(List(1, 2, 3), List(11, 22, 33)))

  // usage of validation
  case class WebForm(name: String, birthdate: Date, phoneNumber: String)
  val birthdateErrMsg = "Birthdate must be in the form yyyy-MM-dd"
  val phoneErrMsg = "Phone number must be 10 digits"
  def validName(name: String): Validation[String, String] =
    if (name != "") Success(name)
    else Failure("Name cannot be empey")
  def validBirthdate(birthdate: String): Validation[String, Date] =
    try {
      import java.text._
      Success((new SimpleDateFormat("yyyy-MM-dd")).parse(birthdate))
    } catch {
      case _: Throwable => Failure(birthdateErrMsg)
    }
  def validPhone(phoneNumber: String): Validation[String, String] =
    if (phoneNumber.matches("[0-9]{10}"))
      Success(phoneNumber)
    else Failure(phoneErrMsg)
  def validWebForm(name: String, birthdate: String, phone: String): Validation[String, WebForm] =
    validationApplicative.map3(
        validName(name),
        validBirthdate(birthdate),
        validPhone(phone))(WebForm(_, _, _))
  val v1 = validWebForm("foo", "2015-07-11", "0123456789")
  assert(v1 match {
    case Success(_) => true
    case _ => false
  })
  val v2 = validWebForm("foo", "xxxx-yy-zz", "abcde")
  assert(v2 match {
    case Failure(h, t) => h +: t == Vector(birthdateErrMsg, phoneErrMsg)
    case _ => false
  })

  // 12.15
  // It is becaouse that Foldable instances have the ability to aggregate values,
  // but not have the ability to preserve a structure.

  // 12.16
  val l1 = listTraverse.reverse(List(3, 2, 1))
  assert(l1 == List(1, 2, 3))

  val l2 = listTraverse.reverse(List(6, 5, 4))
  assert(listTraverse.reverse(List(6, 5, 4) ++ List(3, 2, 1)) == l1 ++ l2)

  // 12.17
  assert(listTraverse.foldLeft(List(1, 2, 3, 4, 5))(0)((_ + _)) == 15)

  // 12.19
  val l3 = List(Some(1), None, Some(2), None, Some(3), None)
  assert(listTraverse.compose(optionTraverse).foldLeft(l3)(0)(_ + _) == 6)
}