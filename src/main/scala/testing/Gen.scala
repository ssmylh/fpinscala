package testing

import state._
import parallelism._
import parallelism.Par.Par

case class Gen[+A](sample: State[RNG, A]) {
  // 8.6
  def flatMap[B](f: A => Gen[B]): Gen[B] = Gen(sample.flatMap(f(_).sample))
  def listOfN(size: Gen[Int]): Gen[List[A]] =
    size.flatMap(n => Gen.listOfN(n, this))

  // 8.10
  def unsized: SGen[A] = SGen(_ => this)

  def map[B](f: A => B): Gen[B] = Gen(sample.map(f))
  def map2[B, C](g: Gen[B])(f: (A, B) => C): Gen[C] = Gen(sample.map2(g.sample)(f))
  def **[B](g: Gen[B]): Gen[(A, B)] = (this map2 g)((_, _))
}

case class SGen[+A](forSize: Int => Gen[A]) {
  // 8.11
  def flatMap[B](f: A => SGen[B]): SGen[B] = SGen(n => {
    forSize(n).flatMap(f(_).forSize(n))
  })
}

object Gen {
  // 8.4
  def choose(start: Int, stopExclusive: Int): Gen[Int] =
    Gen(State(RNG.nonNegativeInt).map(start + _ % (stopExclusive - start)))

  // 8.5
  def unit[A](a: => A): Gen[A] = Gen(State(RNG.unit(a)))
  def boolean: Gen[Boolean] = Gen(State(RNG.nonNegativeInt).map(_ % 2 == 0))
//  def listOfN1[A](n: Int, g: Gen[A]): Gen[List[A]] = Gen(State(rng => {
//    def go(i: Int, r: RNG, acc: List[A]): (List[A], RNG) = {
//      if (i <= 0) (acc, r)
//      else {
//        val (a, r1) = g.sample.run(r)
//        go(i - 1, r1, a :: acc)
//      }
//    }
//    val (l, r) = go(n, rng, List())
//    (l.reverse, r)
//  }))
  def listOfN[A](n: Int, g: Gen[A]): Gen[List[A]] = Gen(State.sequence(List.fill(n)(g.sample)))

  // 8.7
  def union[A](g1: Gen[A], g2: Gen[A]): Gen[A] = boolean.flatMap(if (_) g1 else g2)

  // 8.8
  def weighted[A](g1: (Gen[A], Double), g2: (Gen[A], Double)): Gen[A] = Gen(State(RNG.double)).flatMap { d =>
    val g1Weight = g1._2 / (g1._2 + g2._2)
    if (g1Weight <= d) g1._1 else g2._1
  }

  // 8.12
  def listOf[A](g: Gen[A]): SGen[List[A]] = SGen(n => listOfN(n, g))

  // 8.13
  def listOf1[A](g: Gen[A]): SGen[List[A]] = SGen(n => listOfN(n max 1, g))

  object ** {
    def unapply[A, B](p: (A, B)) = Some(p)
  }

  val pint: Gen[Par[Int]] = choose(0,10) map (Par.unit(_))
}