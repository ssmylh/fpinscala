package testing

import state._
import parallelism._

// This program does *not* finish naturally because of using `ExecutorService` in Prop object.
object Exercise extends App {
  val rng = SimpleRNG(100)

  // 8.4
  val g1 = Gen.choose(10, 100)
  val t1 = g1.sample.run(rng)
  assert(t1._1 >= 10)
  assert(t1._1 < 100)

  // 8.5
  val t3 = Gen.listOfN(50, g1).sample.run(rng)
  assert(t3._1.length == 50)
  t3._1.foreach { n =>
    assert(n >= 10)
    assert(n < 100)
  }

  // 8.6
  val g2 = Gen.choose(5, 10)
  val t2 = g1.listOfN(g2).sample.run(rng)
  assert(t2._1.length >= 5)
  assert(t2._1.length < 10)

  // 8.12
  val sg = Gen.listOf(g1)
  val t4 = sg.forSize(50).sample.run(rng)
  assert(t4._1.length == 50)
  t4._1.foreach { n =>
    assert(n >= 10)
    assert(n < 100)
  }

  // 8.13
  val smallInt = Gen.choose(-10, 10)
  val maxProp = Prop.forAll(Gen.listOf1(smallInt)){ ns =>
    val max = ns.max
    !ns.exists(_ > max)
  }
  Prop.run(maxProp, 5, 16)

  // 8.14
  val sortedProp = Prop.forAll(Gen.listOf(smallInt)) { ns =>
    val sorted = ns.sorted
    (ns.isEmpty || sorted.tail.isEmpty || !sorted.zip(sorted.tail).exists { case (x, y) => x > y }) &&
      !ns.exists(!sorted.contains(_)) &&
      !sorted.exists(!ns.contains(_))
  }
  Prop.run(sortedProp, 5, 16)

  // 8.17
  val forkProp = Prop.forAllPar(Gen.pint)(i => Prop.equal(Par.fork(i), i))
  Prop.run(forkProp, 5, 16)

  // 8.18
  val takeWhileProp = Prop.forAll(Gen.listOf(smallInt)) { ns =>
    val taken = ns.takeWhile(_ > 0)
    val takenLen = taken.length
    if (takenLen == ns.length) true
    else {
      ns(takenLen) <= 0
    }
  }
  Prop.run(takeWhileProp, 5, 16)
  val relationBetweenTakeWhileAndDropWhile = Prop.forAll(Gen.listOf(smallInt)) { ns =>
    val taken = ns.takeWhile(_ > 0)
    val dropped = ns.dropWhile(_ > 0)
    ns == taken ++ dropped
  }
  Prop.run(relationBetweenTakeWhileAndDropWhile, 5, 16)
}