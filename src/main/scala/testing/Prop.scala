package testing

import java.util.concurrent._

import Gen._
import Prop._
import laziness._
import parallelism.Par
import parallelism.Par.Par
import state._

case class Prop(run: (MaxSize, TestCases, RNG) => Result) {
  // 8.9
  def &&(p: Prop): Prop = Prop {
    (mc, tc, rng) => run(mc, tc, rng) match {
      case Passed | Proved => p.run(mc, tc, rng)
      case f => f
    }
  }
  def ||(p: Prop): Prop = Prop {
    (mc, tc, rng) => run(mc, tc, rng) match {
      case Falsified(fc, _) => p.prepend(fc).run(mc, tc, rng)
      case s => s
    }
  }
  def prepend(msg: String): Prop = Prop {
    (mc, tc, rng) => run(mc, tc, rng) match {
      case Falsified(fc, sc) => Falsified(msg + "\n" + fc, sc)
      case s => s
    }
  }
}

object Prop {
  type MaxSize = Int
  type FailedCase = String
  type SuccessCount = Int
  type TestCases = Int

  sealed trait Result {
    def isFalsified: Boolean
  }
  case object Passed extends Result {
    def isFalsified = false
  }
  case class Falsified(failure: FailedCase, successes: SuccessCount) extends Result {
    def isFalsified = true
  }
  case object Proved extends Result {
    def isFalsified = false
  }

  // for adding MaxSize to Prop case class.
  def apply(run: (TestCases, RNG) => Result): Prop = Prop((_, tc, rng) => run(tc, rng))

  def forAll[A](as: Gen[A])(f: A => Boolean): Prop = Prop {
    (n, rng) =>
      randomStream(as)(rng).zip(Stream.from(0)).take(n).map {
        case (a, i) => try {
          if (f(a)) Passed else Falsified(a.toString, i)
        } catch { case e: Exception => Falsified(buildMsg(a, e), i) }
      }.find(_.isFalsified).getOrElse(Passed)
  }

  def randomStream[A](g: Gen[A])(rng: RNG): Stream[A] =
    Stream.unfold(rng)(rng => Some(g.sample.run(rng)))

  def buildMsg[A](a: A, e: Exception): String =
    s"test case: $a\n" +
      s"generated an exception: ${e.getMessage}\n" +
      s"stack trace:\n ${e.getStackTrace.mkString("\n")}"

  def forAll[A](sg: SGen[A])(f: A => Boolean): Prop = forAll(n => sg.forSize(n))(f)

  /*
   * e.g
   *
   * casesPerSize = 1 + (n - 1)/max
   *
   * g : Int => Gen[List[Int]]
   * n = 16
   * max = 5
   * casesPerSize = 1 + (16 - 1)/5 = 4
   *
   * size of list   : 0, 1, 2, 3, 4
   * cases per size : 4, 4, 4, 4, 4
   *
   * generates total 20 cases
   */
  def forAll[A](g: Int => Gen[A])(f: A => Boolean): Prop = Prop { (max, n, rng) =>
    val casesPerSize = (n + (max - 1)) / max
    val props: Stream[Prop] =
      Stream.from(0).take(n min max).map(i => forAll(g(i))(f))
    val prop: Prop = props.map(p =>
      Prop { (max, _, rng) => p.run(max, casesPerSize, rng)
      }).toList.reduce(_ && _)
    prop.run(max, n, rng)
  }

  def run(p: Prop,
    maxSize: Int = 100,
    testCases: Int = 100,
    rng: RNG = SimpleRNG(System.currentTimeMillis)): Unit = {
    p.run(maxSize, testCases, rng) match {
      case Falsified(msg, n) => println(s"! Falsified after $n passed tests:\n $msg")
      case Passed => println(s"+ OK, passed $testCases tests.")
      case Proved => println(s"+ OK, proved property.")
    }
  }

  val ES: ExecutorService = Executors.newCachedThreadPool

  def check(b: => Boolean): Prop = Prop { (_, _, _) =>
    if (b) Passed else Falsified("()", 0)
  }
  val p2 = check {
    val p1 = Par.map(Par.unit(1))(_ + 1)
    val p2 = Par.unit(2)
    p1(ES).get == p2(ES).get
  }

  def equal[A](p1: Par[A], p2: Par[A]): Par[Boolean] = Par.map2(p1, p2)(_ == _)
  val p3 = check {
    equal(
      Par.map(Par.unit(1))(_ + 1),
      Par.unit(2)
    )(ES).get
  }

  val S = weighted(
    choose(1, 4).map(Executors.newFixedThreadPool) -> .75,
    unit(Executors.newCachedThreadPool) -> .25)
  def forAllPar[A](g: Gen[A])(f: A => Par[Boolean]): Prop =
    //forAll(S.map2(g)((_, _))) { case (s, a) => f(a)(s).get }
    forAll(S ** g) { case s ** a => f(a)(s).get }
}