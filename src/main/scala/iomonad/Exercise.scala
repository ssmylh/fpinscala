package iomonad

import Free._

object Exercise extends App {
  {
    import IO1._, IO._
    assert(factorial(5).run == 120)
  }

  {
    import IO2._
    val f: Int => IO[Int] = x => Return(x)
    val g = List.fill(100000)(f).foldLeft(f) {
      // if the following code, SOE occurs.
      // (x: Int) => acc(x).flatMap(_f)
      (acc, _f) => (x: Int) => Suspend(() => ()).flatMap { _ => acc(x).flatMap(_f) }
    }
    assert(run(g(10)) == 10)
  }

  {
    type TailRec[A] = Free[Function0, A]
    val f: Int => TailRec[Int] = x => Return(x)
    val g: Int => TailRec[Int] = List.fill(100000)(f).foldLeft(f) {
      (acc, _f) => (x: Int) => Suspend(() => ()).flatMap { _ => acc(x).flatMap(_f) }
    }
    assert(runTrampoline(g(10)) == 10)
  }

  {
    // List 13-20
    import IO3._, Console._

    val msg = "I can only interact with the console."
    val f1: Free[Console, Option[String]] = for {
      _ <- printLn(msg)
      ln <- readLn
    } yield ln
    //println(runConsoleFunction0(f1)())// input "abc", then print Some("abc")

    // List 13-21, 13-22
    val r1 = runConsoleReader(f1)
    assert(r1.run("foo") == Some("foo"))

    // List 13-23
    val s1 = runConsoleState(f1)
    val in = "foo"
    val (a, s) = s1.run(Buffers(List(in), Vector()))
    assert(a == Some(in))
    assert(s.in == List())
    assert(s.out == Vector(msg))
  }
}