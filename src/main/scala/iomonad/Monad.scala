package iomonad

import scala.language.higherKinds

trait Monad[F[_]] {
  def unit[A](a: => A): F[A]
  def flatMap[A, B](fa: F[A])(f: A => F[B]): F[B]

  def map[A, B](fa: F[A])(f: A => B): F[B] = flatMap(fa)(a => unit(f(a)))
  def skip[A](a: F[A]): F[Unit] = as(a)(())
  def as[A, B](a: F[A])(b: B): F[B] = map(a)(_ => b)

  def foreachM[A](as: Stream[A])(f: A => F[Unit]): F[Unit] =
    foldM(as)(())((u, a) => skip(f(a)))
  def foldM[A, B](as: Stream[A])(z: B)(f: (B, A) => F[B]): F[B] = as match {
    case h #:: t => flatMap(f(z, h))(b => foldM(t)(b)(f))
    case _ => unit(z)
  }

}