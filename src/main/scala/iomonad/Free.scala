package iomonad

import scala.language.higherKinds
import Translate._

trait Free[F[_], A] {
  // 13.1
  def map[B](f: A => B): Free[F, B] = flatMap(f andThen Return.apply)
  def flatMap[B](f: A => Free[F, B]): Free[F, B] = FlatMap(this, f)
}
case class Return[F[_], A](a: A) extends Free[F, A]
case class Suspend[F[_], A](s: F[A]) extends Free[F, A]
case class FlatMap[F[_], A, B](s: Free[F, A], f: A => Free[F, B]) extends Free[F, B]

object Free {
  // 13.1
  def freeMonad[F[_]]: Monad[({ type L[X] = Free[F, X] })#L] = new Monad[({ type L[X] = Free[F, X] })#L] {
    def unit[A](a: => A): Free[F, A] = Return(a)
    def flatMap[A, B](fa: Free[F, A])(f: A => Free[F, B]): Free[F, B] = fa.flatMap(f)
  }

  // 13.2
  @annotation.tailrec
  def runTrampoline[A](fa: Free[Function0, A]): A = fa match {
    case Return(a) => a
    case Suspend(s) => s()
    case FlatMap(x, f) => x match {
      case Return(a) => runTrampoline(f(a))
      case Suspend(s) => runTrampoline(f(s()))
      case FlatMap(y, g) =>
        runTrampoline(y.flatMap(a => g(a).flatMap(f)))// from left associative to right associative
    }
  }

  // 13.3
//  def run[F[_], A](free: Free[F, A])(implicit F: Monad[F]): F[A] = free match {
//    case Return(a) => F.unit(a)
//    case Suspend(s) => s
//    case FlatMap(x, f) => x match {
//      case Return(a) => run(f(a))
//      case Suspend(s) => F.flatMap(s)(a => run(f(a))) // this may cause SOE
//      case FlatMap(y, g) => run(y.flatMap(a => g(a).flatMap(f)))
//    }
//  }
  @annotation.tailrec
  def step[F[_], A](free: Free[F, A]): Free[F, A] = free match {
    case FlatMap(FlatMap(x, f), g) => step(x.flatMap(a => f(a).flatMap(g)))
    case FlatMap(Return(a), f) => step(f(a))
    case _ => free
  }
  def run[F[_], A](free: Free[F, A])(implicit F: Monad[F]): F[A] = step(free) match {
    case Return(a) => F.unit(a)
    case Suspend(s) => s
    case FlatMap(x, f) => x match {
      case Suspend(s) => F.flatMap(s)(a => run(f(a)))
      case _ => sys.error("Impossible; `step` eliminates these cases")
    }
  }

  def runFree[F[_], G[_], A](free: Free[F, A])(t: F ~> G)(implicit G: Monad[G]): G[A] = step(free) match {
    case Return(a) => G.unit(a)
    case Suspend(s) => t(s)
    case FlatMap(Suspend(s), f) => G.flatMap(t(s))(a => runFree(f(a))(t))
    case _ => sys.error("Impossible; `step` eliminates these cases")
  }
}