package iomonad

import scala.language.postfixOps
import scala.language.higherKinds

object IO1 {
  sealed trait IO[A] { self =>
    def run: A
    def map[B](f: A => B): IO[B] = new IO[B] {
      def run = f(self.run)
    }
    def flatMap[B](f: A => IO[B]): IO[B] = new IO[B] {
      def run = f(self.run).run
    }
  }

  object IO extends Monad[IO] {
    def unit[A](a: => A): IO[A] = new IO[A] { def run = a }
    def flatMap[A, B](fa: IO[A])(f: A => IO[B]) = fa flatMap f

    def apply[A](a: => A): IO[A] = unit(a)
    def readLine: IO[String] = IO { io.StdIn.readLine }
    def printLine(msg: String): IO[Unit] = IO { println(msg) }

    class IORef[A](var value: A) {
      def set(a: A): IO[A] = IO {
        value = a
        a
      }
      def get: IO[A] = IO(value)
      def modify(f: A => A): IO[A] = get flatMap (a => set(f(a))) // return IO[A] and modify `value` of itself.
    }
    def ref[A](a: A): IO[IORef[A]] = IO(new IORef(a))

    def factorial(n: Int): IO[Int] = for {
      acc <- ref(1)
      _ <- foreachM(1 to n toStream)(i => skip(acc.modify(_ * i))) // repeat modifying `value` of `acc`.
      result <- acc.get
    } yield result
  }
}

object IO2 {
  sealed trait IO[A] { self =>
    def map[B](f: A => B): IO[B] = flatMap(f andThen (Return(_)))
    def flatMap[B](f: A => IO[B]): IO[B] = FlatMap(this, f)
  }
  case class Return[A](a: A) extends IO[A]
  case class Suspend[A](resume: () => A) extends IO[A]
  case class FlatMap[A, B](sub: IO[A], f: A => IO[B]) extends IO[B]

  @annotation.tailrec
  def run[A](io: IO[A]): A = io match {
    case Return(a) => a
    case Suspend(r) => r()
    case FlatMap(x, f) => x match {
      case Return(a) => run(f(a))
      case Suspend(r) => run(f(r()))
      case FlatMap(y, g) => run(y flatMap (a => g(a) flatMap f))// use associativity
    }
  }

  object IO extends Monad[IO] {
    def unit[A](a: => A): IO[A] = Return(a)
    def flatMap[A, B](fa: IO[A])(f: A => IO[B]) = fa flatMap f
    def apply[A](a: => A): IO[A] = Suspend(() => a) // provide the `IO { ... }` syntax.
  }
}

object IO3 {
  import Free._
  import parallelism._, Par._
  import Translate._
  import Console._

  sealed trait Console[A] {
    def toPar: Par[A]
    def toThunk: () => A
    def toReader: ConsoleReader[A]
    def toState: ConsoleState[A]
  }
  case object ReadLine extends Console[Option[String]] {
    def toPar: Par[Option[String]] = Par.lazyUnit(run)
    def toThunk: () => Option[String] = () => run
    def toReader: ConsoleReader[Option[String]] = ConsoleReader(s => Some(s))
    def toState: ConsoleState[Option[String]] = ConsoleState(buf => buf.in match {
      case h :: t => (Some(h), buf.copy(in = t))
      case Nil => (None, buf)
    })

    def run: Option[String] =
      try Some(io.StdIn.readLine())
      catch { case e: Exception => None }
  }
  case class PrintLine(line: String) extends Console[Unit] {
    def toPar: Par[Unit] = Par.lazyUnit(println(line))
    def toThunk: () => Unit = () => println(line)
    def toReader: ConsoleReader[Unit] = ConsoleReader(s => ())
    def toState: ConsoleState[Unit] = ConsoleState(buf => ((), buf.copy(out = buf.out :+ line)))
  }

  object Console {
    type ConsoleIO[A] = Free[Console, A]
    def readLn: ConsoleIO[Option[String]] = Suspend(ReadLine)
    def printLn(line: String): ConsoleIO[Unit] = Suspend(PrintLine(line))
  }

  val console2Function0 = new (Console ~> Function0) {
    def apply[A](a: Console[A]) = a.toThunk
  }
  val console2Par = new (Console ~> Par) {
    def apply[A](a: Console[A]) = a.toPar
  }

  implicit val function0Monad: Monad[Function0] = new Monad[Function0] {
    def unit[A](a: => A): Function0[A] = () => a
    def flatMap[A, B](a: Function0[A])(f: A => Function0[B]): Function0[B] = () => f(a())()// This is not stack safe.
  }
  implicit val parMonad: Monad[Par] = new Monad[Par] {
    def unit[A](a: => A): Par[A] = Par.unit(a)
    def flatMap[A, B](a: Par[A])(f: A => Par[B]): Par[B] = Par.fork { Par.flatMap(a)(f) }
  }
  def runConsoleFunction0[A](a: Free[Console, A]): () => A =
    runFree[Console, Function0, A](a)(console2Function0)// This is not stack safe, because `flatMap` of `function0Monad` is not stack safe.
  def runConsolePar[A](a: Free[Console, A]): Par[A] =
    runFree[Console, Par, A](a)(console2Par)

  // 13.4
  // def runFree[F[_], G[_], A](free: Free[F, A])(t: F ~> G)(implicit G: Monad[G]): G[A]
  //
  // To implement translate using runFree, need to treat `G[A]` of the above `runFree` as `Free[G, A]`.
  def translate[F[_], G[_], A](free: Free[F, A])(t: F ~> G): Free[G, A] = {
    type FreeG[A] = Free[G, A]
    val f2FreeG: F ~> FreeG = new (F ~> FreeG) {
      def apply[A](a: F[A]): FreeG[A] = Suspend(t(a))
    }
    runFree(free)(f2FreeG)(freeMonad[G])
  }
  // From hint, used "runTrampoline[A](fa: Free[Function0, A]): A".
  def runConsole[A](a: Free[Console, A]): A = runTrampoline(translate(a)(console2Function0)) // This does not use `function0Monad`.

  // List 13-21
  import ConsoleReader._
  case class ConsoleReader[A](run: String => A) {
    def map[B](f: A => B): ConsoleReader[B] = ConsoleReader(s => f(run(s)))
    def flatMap[B](f: A => ConsoleReader[B]): ConsoleReader[B] = ConsoleReader(s => f(run(s)).run(s))
  }
  object ConsoleReader {
    implicit val monad: Monad[ConsoleReader] = new Monad[ConsoleReader] {
      def unit[A](a: => A) = ConsoleReader(_ => a)
      def flatMap[A, B](fa: ConsoleReader[A])(f: A => ConsoleReader[B]) = fa.flatMap(f)
    }
  }
  // List 13-22
  val console2Reader = new (Console ~> ConsoleReader) {
    def apply[A](a: Console[A]) = a.toReader
  }
  def runConsoleReader[A](io: ConsoleIO[A]): ConsoleReader[A] =
    runFree[Console, ConsoleReader, A](io)(console2Reader)

  // List 13-23
  import ConsoleState._
  case class Buffers(in: List[String], out: Vector[String])
  case class ConsoleState[A](run: Buffers => (A, Buffers)) {
    def map[B](f: A => B): ConsoleState[B] = ConsoleState(buf1 => {
      val (a, buf2) = run(buf1)
      (f(a), buf2)
    })
    def flatMap[B](f: A => ConsoleState[B]): ConsoleState[B] = ConsoleState(buf1 => {
      val (a, buf2) = run(buf1)
      f(a).run(buf2)
    })
  }
  object ConsoleState {
    implicit val monad: Monad[ConsoleState] = new Monad[ConsoleState] {
      def unit[A](a: => A) = ConsoleState(buf => (a, buf))
      def flatMap[A, B](fa: ConsoleState[A])(f: A => ConsoleState[B]) = fa.flatMap(f)
    }
  }
  val console2State = new (Console ~> ConsoleState) {
    def apply[A](a: Console[A]) = a.toState
  }
  def runConsoleState[A](io: ConsoleIO[A]): ConsoleState[A] =
    runFree[Console, ConsoleState, A](io)(console2State)
}
