package localeffects

import ST._

object Exercise extends App {
  val p = new RunnableST[(Int, Int)] {
    def apply[S]: ST[S, (Int, Int)] = for {
      r1 <- STRef(1)
      r2 <- STRef(2)
      x <- r1.read
      y <- r2.read
      _ <- r1.write(y + 1)
      _ <- r2.write(x + 1)
      a <- r1.read
      b <- r2.read
    } yield (a, b)
  }
  assert(runST(p) == (3, 2))

  // compilation error
  // found    : ST[S, STRef[S, Int]]
  // required : ST[S, STRef[Nothing, Int]]
  //
  // Since `STRef.apply` return `ST[S, STRef[S, A]]`, `S` of `STRef` is binded to `S` of outer `ST`.
  // BTW `S` of `ST` is binded to `S` of `Runnable.apply`.
  // That is, the above compilation error causes.
//  new RunnableST[STRef[Nothing, Int]] {
//    def apply[S] = STRef(1)
//  }

  // List 14-6
  val p2 = new RunnableST[List[Int]] {
    def apply[S]: ST[S, List[Int]] = for {
      a1 <- STArray(3, 0)
      _ <- a1.write(0, 1)
      _ <- a1.write(1, 2)
      _ <- a1.write(2, 3)
      l <- a1.freeze
    } yield l
  }
  assert(runST(p2) == List(1, 2, 3))

  // 14.1
  val p3 = new RunnableST[List[Int]] {
    def apply[S]: ST[S, List[Int]] = for {
      a1 <- STArray(3, 0)
      _ <- a1.fill(Map(0 -> 1, 1 -> 2, 2 -> 3))
      l <- a1.freeze
    } yield l
  }
  assert(runST(p3) == List(1, 2, 3))

  // 14.2
  def partition[S](a: STArray[S, Int], l: Int, r: Int, pivot: Int): ST[S, Int] = {
    val stUnit: ST[S, Unit] = ST(())
    for {
      pivotVal <- a.read(pivot)
      _ <- a.swap(pivot, r)
      refj <- STRef(l)
      _ <- (l until r).foldLeft(stUnit)((acc, i) => for {
        _ <- acc // execute !
        v <- a.read(i)
        _ <- if (v < pivotVal) {
          for {
            j <- refj.read
            _ <- a.swap(i, j)
            _ <- refj.write(j + 1)
          } yield ()
        } else stUnit
      } yield ())
      j <- refj.read
      _ <- a.swap(j, r)
    } yield j
  }

  def qs[S](a: STArray[S, Int], l: Int, r: Int): ST[S, Unit] =
    if (l < r) {
      for {
        pi <- partition(a, l, r, l + (r - l) / 2)
        _ <- qs(a, l, pi - 1)
        _ <- qs(a, pi + 1, r)
      } yield ()
    } else ST(())

  def quicksort(xs: List[Int]): List[Int] =
    if (xs.isEmpty) xs
    else {
      ST.runST(new RunnableST[List[Int]] {
        def apply[S] = for {
          arr <- STArray.fromList(xs)
          size <- arr.size
          _ <- qs(arr, 0, size - 1)
          sorted <- arr.freeze
        } yield sorted
      })
    }
  assert(quicksort(List()) == List())
  assert(quicksort(List(6, 1, 8, 2, 9, 5, 4, 7, 3)) == (1 to 9).toList)

  // 14.3
  val p4 = new RunnableST[Map[Symbol, Int]] {
    def apply[S]: ST[S, Map[Symbol, Int]] = for {
      m1 <- STMap.empty[S, Symbol, Int]
      _ <- m1 += ('a -> 1)
      _ <- m1 += ('b -> 3)
      _ <- m1 += ('b -> 2)
      a <- m1.get('a)
      b <- m1.get('b)
      _ <- m1 += ('c -> (a.getOrElse(0) + b.getOrElse(0)))
      _ <- m1 += ('d -> 4)
      _ <- m1 -= 'd
      m2 <- m1.freeze
    } yield m2
  }
  assert(runST(p4) == Map('a -> 1, 'b -> 2, 'c -> 3))
}