package localeffects

sealed trait ST[S, A] { self =>
  protected def run(s: S): (A, S)
  def map[B](f: A => B): ST[S, B] = new ST[S, B] {
    def run(s: S) = {
      val (a, s1) = self.run(s)
      (f(a), s1)
    }
  }
  def flatMap[B](f: A => ST[S, B]): ST[S, B] = new ST[S, B] {
    def run(s: S) = {
      val (a, s1) = self.run(s)
      f(a).run(s1)
    }
  }
}

object ST {
  def apply[S, A](a: => A): ST[S, A] = {
    lazy val memo = a
    new ST[S, A] {
      def run(s: S): (A, S) = (memo, s)
    }
  }
  def runST[A](r: RunnableST[A]): A = r.apply[Unit].run(())._1
}

trait RunnableST[A] {
  def apply[S]: ST[S, A]
}

sealed trait STRef[S, A] {
  protected var cell: A
  def read: ST[S, A] = ST(cell)
  def write(a: A): ST[S, Unit] = new ST[S, Unit] {
    def run(s: S): (Unit, S) = {
      cell = a
      ((), s)
    }
  }
}

object STRef {
  def apply[S, A](a: A): ST[S, STRef[S, A]] = ST(new STRef[S, A] {
    var cell = a
  })
}

sealed abstract class STArray[S, A](implicit manifest: Manifest[A]) {
  protected def value: Array[A]
  def size: ST[S, Int] = ST(value.size)
  def write(i: Int, a: A): ST[S, Unit] = new ST[S, Unit] {
    def run(s: S): (Unit, S) = {
      value(i) = a
      ((), s)
    }
  }
  def read(i: Int): ST[S, A] = ST(value(i))
  def freeze: ST[S, List[A]] = ST(value.toList)

  // 14.1
  def fill(kv: Map[Int, A]): ST[S, Unit] = new ST[S, Unit] {
    def run(s: S): (Unit, S) = {
      kv.foreach { case (k, v) => value(k) = v }
      ((), s)
    }
  }

  def swap(i: Int, j: Int): ST[S, Unit] = for {
    x <- read(i)
    y <- read(j)
    _ <- write(i, y)
    _ <- write(j, x)
  } yield ()
}

object STArray {
  def apply[S, A: Manifest](sz: Int, v: A): ST[S, STArray[S, A]] = ST(new STArray[S, A] {
    lazy val value = Array.fill(sz)(v)
  })

  def fromList[S, A: Manifest](xs: List[A]): ST[S, STArray[S, A]] = ST(new STArray[S, A] {
    lazy val value = xs.toArray
  })
}

// 14.3
import scala.collection.mutable.HashMap
sealed abstract class STMap[S, K, V] {
  protected def value: HashMap[K, V]
  def size: ST[S, Int] = ST(value.size)
  def +=(kv: (K, V)): ST[S, Unit] = new ST[S, Unit] {
    def run(s: S): (Unit, S) = {
      value += kv
      ((), s)
    }
  }
  def -=(k: K): ST[S, Unit] = new ST[S, Unit] {
    def run(s: S): (Unit, S) = {
      value -= k
      ((), s)
    }
  }
  def get(k: K): ST[S, Option[V]] = ST(value.get(k))
  def freeze: ST[S, Map[K, V]] = ST(value.toMap)
}

object STMap {
  def empty[S, K, V]: ST[S, STMap[S, K, V]] = ST(new STMap[S, K, V] {
    val value = HashMap[K, V]()
  })
}
