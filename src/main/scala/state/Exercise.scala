package state

object Exercise extends App {
  val rng = SimpleRNG(100)

  // 6.5
  assert(RNG.double(rng) == RNG.doubleViaMap(rng))

  // 6.9
  assert(RNG.nonNegativeLessThan(10)(rng)._1 >= 0)
  assert(RNG.nonNegativeLessThan(10)(rng)._1 < 10)

  // 6.11
  sealed trait Input
  case object Coin extends Input
  case object Turn extends Input
  case class Machine(locked: Boolean, snacks: Int, coins: Int)

  object Machine {
    import State._

    def simulateMachine(inputs: List[Input]): State[Machine, (Int, Int)] = {
      val sms: List[State[Machine, Unit]] = inputs.map(i => modify((m: Machine) => input(m, i)))
      for {
        _ <- sequence(sms)
        s <- get
      } yield (s.snacks, s.coins)
    }

    def input(m: Machine, i: Input): Machine = (m, i) match {
      case (Machine(true, s, c), Coin) if s > 0 => Machine(false, s, c + 1)
      case (Machine(false, s, c), Turn) => Machine(true, s - 1, c)
      case (Machine(true, _, _), Turn) => m
      case (Machine(false, _, _), Coin) => m
      case (Machine(_, 0, _), _) => m
    }
  }

  val sm = Machine.simulateMachine(List(Coin, Turn, Coin, Turn, Coin, Turn, Coin, Turn))
  val m = Machine(true, 5, 10)
  val r = sm.run(m)
  assert(r._1._1 == 1)
  assert(r._1._2 == 14)
}