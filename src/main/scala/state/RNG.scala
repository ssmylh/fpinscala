package state

trait RNG {
  def nextInt: (Int, RNG)
}

object RNG {
  // 6.1
  def nonNegativeInt(rng: RNG): (Int, RNG) = {
    val (i, r) = rng.nextInt
    if (i < 0) (-(i + 1), r)
    else (i, r)
  }

  // 6.2
  def double(rng: RNG): (Double, RNG) = {
    val (i, r) = nonNegativeInt(rng)
    (i / (Int.MaxValue.toDouble + 1), r)
  }

  // 6.3
  def intDouble(rng: RNG): ((Int, Double), RNG) = {
    val (i, r1) = rng.nextInt
    val (d, r2) = double(r1)
    ((i, d), r2)
  }
  def doubleInt(rng: RNG): ((Double, Int), RNG) = {
    val (d, r1) = double(rng)
    val (i, r2) = r1.nextInt
    ((d, i), r2)
  }
  def double3(rng: RNG): ((Double, Double, Double), RNG) = {
    val (d1, r1) = double(rng)
    val (d2, r2) = double(r1)
    val (d3, r3) = double(r2)
    ((d1, d2, d3), r3)
  }

  // 6.4
  def ints(count: Int)(rng: RNG): (List[Int], RNG) = {
    if (count <= 0)
      (List(), rng)
    else {
      val (i, r1) = rng.nextInt
      val (is, r2) = ints(count - 1)(rng)
      (i :: is, r2)
    }
  }

  type Rand[+A] = RNG => (A, RNG)
  val int: Rand[Int] = _.nextInt

  def unit[A](a: A): Rand[A] = rng => (a, rng)

  def map[A, B](ra: Rand[A])(f: A => B): Rand[B] = rng => {
    val (a, r1) = ra(rng)
    (f(a), r1)
  }

  // 6.5
  def doubleViaMap: Rand[Double] =
    map(nonNegativeInt)(_ / (Int.MaxValue.toDouble + 1))

  // 6.6
  def map2[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] = rng => {
    val (a, r1) = ra(rng)
    val (b, r2) = rb(r1)
    (f(a, b), r2)
  }

  def both[A, B](ra: Rand[A], rb: Rand[B]): Rand[(A, B)] = map2(ra, rb)((_, _))

  // 6.7
//  def sequence[A](fs: List[Rand[A]]): Rand[List[A]] = fs match {
//    case Nil => unit(Nil)
//    case r :: rs => rng => {
//      val (a, r1) = r(rng)
//      val ras = sequence1(rs)
//      val (as, r2) = ras(r1)
//      (a :: as, r2)
//    }
//  }
  def sequence[A](ras: List[Rand[A]]): Rand[List[A]] = ras.foldRight(unit(List[A]())){ (ra, acc) =>
    map2(ra, acc)(_ :: _)
  }
  def intsViaSequence(count: Int): Rand[List[Int]] = sequence(List.fill(count)(int))

  // 6.8
  def flatMap[A, B](ra: Rand[A])(g: A => Rand[B]): Rand[B] = rng => {
    val (a, r1) = ra(rng)
    g(a)(r1)
  }
  def nonNegativeLessThan(n: Int): Rand[Int] = flatMap(nonNegativeInt) { i =>
    // In the range of 0 to xn, the remainder of its devision appear eaually, so I should use that range.
    // Only if Int.MaxValue is the multiple of n, I can include xn+1 and over.

    // x = i - mod (x is the multiple of n)
    // if x is the largest multiple of n,  x + (n - 1) is negative.
    val mod = i % n
    if (i - mod + (n - 1) >= 0) unit(mod)
    else nonNegativeLessThan(n)
  }

  // 6.9
  def mapViaFlatMap[A, B](ra: Rand[A])(f: A => B): Rand[B] = flatMap(ra)(a => unit(f(a)))
  def map2ViaFlatMap[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] = flatMap(ra) { a =>
    map(rb)(b => f(a, b))
  }
}

case class SimpleRNG(seed: Long) extends RNG {
  def nextInt: (Int, RNG) = {
    val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
    val nextRNG = SimpleRNG(newSeed)
    val n = (newSeed >>> 16).toInt
    (n, nextRNG)
  }
}