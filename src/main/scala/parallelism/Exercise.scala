package parallelism

import java.util.concurrent._

object Exercise extends App {
  val es = Executors.newFixedThreadPool(10)
  try {

    // 7.5
    val p1 = Par.parMap(List(1, 2, 3))(_ * 2)
    assert(Par.run(es)(p1).get == List(2, 4, 6))

    // 7.6
    val p2 = Par.parFilter(List(1, 2, 3, 4))(_ % 2 == 0)
    assert(Par.run(es)(p2).get == List(2, 4))

    // 7.7
    // map(map(y)(g))(f) == map(y)(f compose g)
    // map(map(y)(id))(f) == map(y)(f compose id)
    // map(y)(f) == map(y)(f)

    // 7.8
    // Depending on the situation ( improper size of thread pool ), a dead lock may occur.
    // http://blog.jessitron.com/2014/01/fun-with-pool-induced-deadlock.html

    // 7.11
    val p3 = Par.choice(Par.unit(true))(p1, p2)
    assert(Par.run(es)(p3).get == List(2, 4, 6))

    // 7.13
    val p4 = Par.choiceViaChooser(Par.unit(true))(p1, p2)
    assert(Par.run(es)(p3).get == List(2, 4, 6))

    // 7.14
    val p5 = Par.flatMap(Par.unit(true))(if(_) p1 else p2)
    assert(Par.run(es)(p3).get == List(2, 4, 6))
  } finally {
    es.shutdown()
  }
}