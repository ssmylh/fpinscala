package parallelism

import java.util.concurrent._
import scala.concurrent.SyncVar

object Par {
  type Par[A] = ExecutorService => Future[A]
  private case class UnitFuture[A](get: A) extends Future[A] {
    def isDone = true
    def get(timeout: Long, units: TimeUnit) = get
    def isCancelled = false
    def cancel(evenIfRunning: Boolean): Boolean = false
  }

  def unit[A](a: A): Par[A] = (es: ExecutorService) => UnitFuture(a)

  def map[A, B](pa: Par[A])(f: A => B): Par[B] = map2(pa, unit(()))((a, _) => f(a))

  def map2[A, B, C](pa: Par[A], pb: Par[B])(f: (A, B) => C): Par[C] =
    (es: ExecutorService) => {
      val af = pa(es)
      val bf = pb(es)
      UnitFuture(f(af.get, bf.get))
    }

  def fork[A](pa: => Par[A]): Par[A] =
    es => es.submit(new Callable[A] {
      def call = pa(es).get
    })

  def delay[A](pa: => Par[A]): Par[A] =
    es => pa(es)

  def run[A](es: ExecutorService)(pa: Par[A]): Future[A] = pa(es)

  def lazyUnit[A](a: => A): Par[A] = fork(unit(a))

  // 7.3
  case class Map2Future[A, B, C](af: Future[A], bf: Future[B], f: (A, B) => C) extends Future[C] {
    private var cache: Option[C] = None
    def isDone = false
    def get(): C = compute(Long.MaxValue)
    def get(timeout: Long, units: TimeUnit): C = compute(TimeUnit.MILLISECONDS.convert(timeout, units))
    def isCancelled = af.isCancelled || bf.isCancelled
    // evaluate both sides.
    def cancel(evenIfRunning: Boolean): Boolean = af.cancel(evenIfRunning) && bf.cancel(evenIfRunning)
    // block explicitly.
    def compute(timeoutMillis: Long): C = synchronized {
      cache match {
        case Some(c) => c
        case None =>
          val start = System.currentTimeMillis
          val a = af.get(timeoutMillis, TimeUnit.MILLISECONDS)
          val aEnd = System.currentTimeMillis
          val b = bf.get(timeoutMillis - (aEnd - start), TimeUnit.MILLISECONDS)
          val c = f(a, b)
          cache = Some(c)
          c
      }
    }
  }

  // 7.4
  def asyncF[A, B](f: A => B): A => Par[B] = a => lazyUnit(f(a))

  // 7.5
  def sequence[A](ps: List[Par[A]]): Par[List[A]] = ps.foldRight(unit(List[A]())) { (pa, acc) =>
    map2(pa, acc)(_ :: _)
  }

  def parMap[A, B](as: List[A])(f: A => B): Par[List[B]] = fork {
    val fbs: List[Par[B]] = as.map(asyncF(f))
    sequence(fbs)
  }

  // 7.6
  def parFilter[A](as: List[A])(f: A => Boolean): Par[List[A]] = fork {
    val pas: List[Par[List[A]]] = as.map(asyncF { (a: A) =>
      if (f(a)) List(a) else List()
    })
    map(sequence(pas))(_.flatten)
  }

  def equal[A](e: ExecutorService)(p: Par[A], p2: Par[A]): Boolean =
    p(e).get == p2(e).get

  // 7.11
  def choiceN[A](n: Par[Int])(choices: List[Par[A]]): Par[A] = es => {
    val i = n(es).get
    choices(i)(es)
  }
  def choice[A](cond: Par[Boolean])(t: Par[A], f: Par[A]): Par[A] =
    choiceN(map(cond)(if (_) 1 else 0))(List(f, t))

  // 7.12
  def choiceMap[K, V](key: Par[K])(choices: Map[K, Par[V]]): Par[V] = es => {
    val k = key(es).get
    choices(k)(es)
  }

  // 7.13
  def chooser[A, B](pa: Par[A])(choices: A => Par[B]): Par[B] = es => {
    val a = pa(es).get
    choices(a)(es)
  }
  def choiceViaChooser[A](cond: Par[Boolean])(t: Par[A], f: Par[A]): Par[A] =
    chooser(cond)(if(_) t else f)
  def chiceNViaChooser[A](n: Par[Int])(choices: List[Par[A]]): Par[A] =
    chooser(n)(choices(_))

  // 7.14
  def join[A](ppa: Par[Par[A]]): Par[A] = es => {
    val pa =  ppa(es).get
    pa(es)
  }
  def flatMap[A, B](pa: Par[A])(f: A => Par[B]): Par[B] = join(map(pa)(f))
  def joinViaFlatMap[A](ppa: Par[Par[A]]): Par[A] = flatMap(ppa)(identity)
  def choiceViaFlatMap[A](cond: Par[Boolean])(t: Par[A], f: Par[A]): Par[A] = flatMap(cond)(if(_) t else f)
}